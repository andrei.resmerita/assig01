package dataflowsAndExceptions.counting;

import java.io.File;

public class CountImgAndPdf {
    public int numberOfFiles(File srcDir) {
        int count = 0;
        File[] listFiles = srcDir.listFiles();
        for (File listFile : listFiles) {
            if (listFile.isDirectory()) {
                count += numberOfFiles(listFile);
            } else if (listFile.getName().endsWith(".pdf") || listFile.getName().endsWith(".img")) {
                count++;
            }
        }
        return count;
    }

}

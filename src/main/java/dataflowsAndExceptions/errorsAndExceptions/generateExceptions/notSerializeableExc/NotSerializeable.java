package dataflowsAndExceptions.errorsAndExceptions.generateExceptions.notSerializeableExc;

import dataflowsAndExceptions.errorsAndExceptions.generateExceptions.nullPointerExc.Person;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class NotSerializeable {

    public void serialize(Person person) {
        try {
            FileOutputStream fileOut =
                    new FileOutputStream("user.txt");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(person);
            out.close();
            fileOut.close();
            System.out.println("Serialized data is saved in person.txt");
        } catch (IOException i) {
            System.out.println("Object not serrializeable");
        }
    }

    public static void main(String[] args) {
        Person p = new Person();
        new NotSerializeable().serialize(p);
    }
}

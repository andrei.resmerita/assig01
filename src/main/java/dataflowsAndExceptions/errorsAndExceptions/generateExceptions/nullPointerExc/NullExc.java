package dataflowsAndExceptions.errorsAndExceptions.generateExceptions.nullPointerExc;

public class NullExc {
    public static void main(String[] args) {
        Person p = new Person();

        try {
            p.doStuff(null);
        } catch (NullPointerException e) {
            System.out.print("Caught NullPointerException");
        }
    }
}

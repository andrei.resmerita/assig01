package dataflowsAndExceptions.errorsAndExceptions.generateExceptions.nullPointerExc;

public class Person {

    private String name;

    public void doStuff(Person o) {
        o.sayHello();
    }

    public String getName() {
        return name;
    }

    public String sayHello() {
        return "SayHello";
    }
}

package dataflowsAndExceptions.errorsAndExceptions.generateExceptions.numberFormatExc;

public class NumberExc {

    public static void main(String[] args) {
        String a = "andrei";
        try {
            Integer.parseInt(a);
        } catch (NumberFormatException e) {
            System.out.println("Number format exception");
        }
    }
}

package dataflowsAndExceptions.errorsAndExceptions.hierarchyalCall;

import dataflowsAndExceptions.errorsAndExceptions.customException.CustomException;

public class GoToStore {
    static String goToStore() {
        try {
            Walk.walk();
        } catch (CustomException e) {
            e.printStackTrace();
        }
        return "I am walking to the store";
    }
}

package dataflowsAndExceptions.errorsAndExceptions.hierarchyalCall;

public class Person {

    private String name;
    private String hairColour;

    public Person() {
    }

    public Person(String name, String hairColour) {
        this.name = name;
        this.hairColour = hairColour;
    }

    String doActivity() {
        return GoToStore.goToStore();
    }

    public static void main(String[] args) {
        Person person = new Person();
        System.out.println(person.doActivity());
    }
}

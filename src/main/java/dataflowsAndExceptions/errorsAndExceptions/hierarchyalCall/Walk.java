package dataflowsAndExceptions.errorsAndExceptions.hierarchyalCall;

import dataflowsAndExceptions.errorsAndExceptions.customException.CustomException;

public class Walk {

    static void walk() throws CustomException {
        try {
            throw new CustomException();
        } catch (CustomException exception) {
            throw new CustomException("An error occured,I can't walk");
        }

    }

}

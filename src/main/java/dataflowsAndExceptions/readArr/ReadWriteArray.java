package dataflowsAndExceptions.readArr;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;

public class ReadWriteArray {
    public String[] readArr(String path) {
        String content = null;
        try {
            content = new String(Files.readAllBytes(Paths.get(path)));
        } catch (IOException e) {
            System.out.println("Cannot find file");
        }
        return content.split(" ");
    }

    public String[] writeReverseArr(String path) {
        String[] oldArr = readArr(path);
        Collections.reverse(Arrays.asList(oldArr));
        return oldArr;
    }


    public static void main(String[] args) throws IOException {
        String path = "src/main/resources/text/file.txt";
        System.out.println(Arrays.toString(new ReadWriteArray().readArr(path)));
        System.out.println(Arrays.toString(new ReadWriteArray().writeReverseArr(path)));
    }
}

package dataflowsAndExceptions.serialize.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.Objects;

@XmlRootElement(name = "user")
@XmlType(propOrder = {"name", "cnp", "address", "city"})
public class User implements Serializable {

    private int id;
    private String name;
    private int cnp;
    private String address;
    private String city;

    public User(String name, int cnp, String address, String city) {
        this.name = name;
        this.cnp = cnp;
        this.address = address;
        this.city = city;
    }

    public User() {
    }

    public int getId() {
        return id;
    }

    @XmlAttribute
    public void setId(int id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    @XmlElement
    public void setName(String name) {
        this.name = name;
    }

    public int getCnp() {
        return cnp;
    }

    @XmlElement
    public void setCnp(int cnp) {
        this.cnp = cnp;
    }

    public String getAddress() {
        return address;
    }

    @XmlElement
    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    @XmlElement
    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", cnp=" + cnp +
                ", address='" + address + '\'' +
                ", city='" + city + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id && cnp == user.cnp && Objects.equals(name, user.name) && Objects.equals(address, user.address) && Objects.equals(city, user.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, cnp, address, city);
    }
}

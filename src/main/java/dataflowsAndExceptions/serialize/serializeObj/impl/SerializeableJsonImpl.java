package dataflowsAndExceptions.serialize.serializeObj.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import dataflowsAndExceptions.serialize.model.User;
import dataflowsAndExceptions.serialize.serializeObj.interfaces.Serializeable;

import java.io.File;
import java.io.FileWriter;

public class SerializeableJsonImpl implements Serializeable {

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void serialize(User user, File file) {

        try {
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(objectMapper.writeValueAsString(user));
            fileWriter.close();
            System.out.println("\n serialization json complete");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public User deserialize(File json) {
        try {
            System.out.println("deserialization json complete. object is: " + (User) this.objectMapper.readValue(json, User.class) + "\n");
            return (User) this.objectMapper.readValue(json, User.class);

        } catch (Exception e) {
            e.printStackTrace();
            return new User();
        }
    }
}

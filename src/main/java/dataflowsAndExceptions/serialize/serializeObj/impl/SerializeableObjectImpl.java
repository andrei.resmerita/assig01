package dataflowsAndExceptions.serialize.serializeObj.impl;

import dataflowsAndExceptions.serialize.model.User;
import dataflowsAndExceptions.serialize.serializeObj.interfaces.Serializeable;

import java.io.*;

public class SerializeableObjectImpl implements Serializeable {
    @Override
    public void serialize(User user, File file) {
        try {
            FileOutputStream fileOut =
                    new FileOutputStream(file);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(user);
            out.close();
            fileOut.close();
            System.out.println("Serialized data is saved in person.txt");
        } catch (IOException i) {
            i.printStackTrace();
        }
    }


    @Override
    public User deserialize(File file) {
        User user = null;
        try {
            FileInputStream fileIn = new FileInputStream(file);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            user = (User) in.readObject();
            System.out.println("Deserialization complete");
            System.out.println("Person name is: " + user.getName() + " city " + user.getCity() + "\n");
            in.close();
            fileIn.close();
        } catch (IOException i) {
            i.printStackTrace();
        } catch (ClassNotFoundException c) {
            System.out.println("Person class not found");
            c.printStackTrace();
        }
        return user;
    }
}

package dataflowsAndExceptions.serialize.serializeObj.impl;

import dataflowsAndExceptions.serialize.model.User;
import dataflowsAndExceptions.serialize.serializeObj.interfaces.Serializeable;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;

public class SerializeableXmlImpl implements Serializeable {
    @Override
    public void serialize(User user, File file) {

        try {
            JAXBContext context = JAXBContext.newInstance(User.class);
            Marshaller mar = context.createMarshaller();
            mar.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            mar.marshal(user, file);
            System.out.println("serialization xml complete");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public User deserialize(File file) {
        try {
            JAXBContext context = JAXBContext.newInstance(User.class);
            System.out.println("Deserialized xml object is : " + (User) context.createUnmarshaller().unmarshal(file));
            return (User) context.createUnmarshaller().unmarshal(file);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}

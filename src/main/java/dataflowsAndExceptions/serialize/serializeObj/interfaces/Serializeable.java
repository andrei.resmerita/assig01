package dataflowsAndExceptions.serialize.serializeObj.interfaces;

import dataflowsAndExceptions.serialize.model.User;

import java.io.File;

public interface Serializeable {

    void serialize(User user, File file);

    User deserialize(File file);

}

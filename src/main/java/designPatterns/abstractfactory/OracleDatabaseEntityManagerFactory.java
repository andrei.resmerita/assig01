package designPatterns.abstractfactory;


import designPatterns.abstractfactory.contracts.AbstractFactory;
import designPatterns.abstractfactory.contracts.EntityManager;
import designPatterns.abstractfactory.model.EntityManangerOracleDatabse;

public class OracleDatabaseEntityManagerFactory extends AbstractFactory {
    @Override
    public EntityManager getEntityManager(String database) {
        return new EntityManangerOracleDatabse();
    }
}

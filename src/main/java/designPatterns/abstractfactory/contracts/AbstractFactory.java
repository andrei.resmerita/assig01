package designPatterns.abstractfactory.contracts;

public abstract class AbstractFactory {

    public abstract EntityManager getEntityManager(String database);

}

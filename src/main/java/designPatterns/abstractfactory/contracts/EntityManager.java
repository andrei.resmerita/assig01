package designPatterns.abstractfactory.contracts;


import designPatterns.abstractfactory.model.Connection;
import designPatterns.abstractfactory.model.Transaction;

public interface EntityManager {


    Connection createConnection(String url, String pass);

    Transaction openTransaction();

    Transaction closeTransaction();

    String convertData();

}

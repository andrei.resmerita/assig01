package designPatterns.abstractfactory.main;

import designPatterns.abstractfactory.MySQLDatabaseEntityManagerFactory;
import designPatterns.abstractfactory.OracleDatabaseEntityManagerFactory;
import designPatterns.abstractfactory.contracts.AbstractFactory;

public class FactoryProducer {

    public AbstractFactory getFactory(String input) {
        if (input.equals("MYSQL")) {
            return new MySQLDatabaseEntityManagerFactory();
        } else if (input.equals("ORACLE")) {
            return new OracleDatabaseEntityManagerFactory();
        }
        return null;
    }


}

package designPatterns.abstractfactory.model;

import java.util.Objects;

public class Connection {
    private String databaseUrl;
    private String databasePass;


    public Connection(String databaseUrl, String databasePass) {
        this.databaseUrl = databaseUrl;
        this.databasePass = databasePass;
    }

    public Connection() {
    }

    public String getDatabaseUrl() {
        return databaseUrl;
    }

    public String getDatabasePass() {
        return databasePass;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Connection that = (Connection) o;
        return Objects.equals(databaseUrl, that.databaseUrl) && Objects.equals(databasePass, that.databasePass);
    }

    @Override
    public int hashCode() {
        return Objects.hash(databaseUrl, databasePass);
    }
}

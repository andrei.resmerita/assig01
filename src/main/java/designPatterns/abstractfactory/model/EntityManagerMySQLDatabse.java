package designPatterns.abstractfactory.model;

import designPatterns.abstractfactory.contracts.EntityManager;

public class EntityManagerMySQLDatabse implements EntityManager {


    @Override
    public Connection createConnection(String url, String pass) {
        return new Connection(url, pass);
    }

    @Override
    public Transaction openTransaction() {
        return new Transaction();
    }

    @Override
    public Transaction closeTransaction() {
        return new Transaction();
    }

    @Override
    public String convertData() {
        return "convert data mysql";
    }
}
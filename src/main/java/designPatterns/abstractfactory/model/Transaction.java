package designPatterns.abstractfactory.model;

import java.util.Objects;

public class Transaction {

    private boolean open;
    private boolean close;

    public Transaction() {
    }

    public Transaction(boolean open, boolean close) {
        this.open = open;
        this.close = close;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return open == that.open && close == that.close;
    }

    @Override
    public int hashCode() {
        return Objects.hash(open, close);
    }

    public boolean isOpen() {
        return open;
    }

    public boolean isClose() {
        return close;
    }
}

package designPatterns.cor;

import designPatterns.cor.contract.Chain;
import designPatterns.cor.model.Account;
import designPatterns.cor.model.ReciverAccount;
import designPatterns.cor.model.SenderAccount;
import designPatterns.cor.model.ValueValidation;

public class Main {

    public static void main(String[] args) {
        Chain c1 = new SenderAccount();

        Chain c2 = new ReciverAccount();

        Chain c3 = new ValueValidation();

        c1.setNextChain(c2);
        c2.setNextChain(c3);

        String action = "send";
        Account request = new Account(1, 3, 100, action);

        System.out.println(c1.validate(request, action));

    }

}

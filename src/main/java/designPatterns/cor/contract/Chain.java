package designPatterns.cor.contract;

import designPatterns.cor.model.Account;

public interface Chain {

    void setNextChain(Chain nextChain);


    String validate(Account request, String message);


}

package designPatterns.cor.model;

public class Account {

    private int accountNumber;
    private int destinatorAccount;
    private int value;
    private String actionWanted;

    public Account(int accountNumber, int destinatorAccount, int value, String actionWanted) {
        this.accountNumber = accountNumber;
        this.destinatorAccount = destinatorAccount;
        this.value = value;
        this.actionWanted = actionWanted;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public int getDestinatorAccount() {
        return destinatorAccount;
    }

    public int getValue() {
        return value;
    }

    public String getActionWanted() {
        return actionWanted;
    }
}

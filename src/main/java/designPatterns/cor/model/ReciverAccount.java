package designPatterns.cor.model;

import designPatterns.cor.contract.Chain;

public class ReciverAccount implements Chain {
    private Chain nextInChain;

    public void setNextChain(Chain nextChain) {

        nextInChain = nextChain;

    }

    public String validate(Account request, String message) {

        if (request.getActionWanted().equals("reciver")) {
            return "reciver";
        }
        return nextInChain.validate(request, request.getActionWanted());
    }
}

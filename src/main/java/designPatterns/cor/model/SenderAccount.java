package designPatterns.cor.model;

import designPatterns.cor.contract.Chain;

public class SenderAccount implements Chain {
    private Chain nextInChain;

    public void setNextChain(Chain nextChain) {

        nextInChain = nextChain;

    }

    public String validate(Account request, String message) {

        if (request.getActionWanted().equals("sender")) {
            return "sender";
        }
        return nextInChain.validate(request, request.getActionWanted());
    }
}

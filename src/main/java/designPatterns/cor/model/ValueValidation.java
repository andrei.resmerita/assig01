package designPatterns.cor.model;

import designPatterns.cor.contract.Chain;

public class ValueValidation implements Chain {

    private Chain nextInChain;

    public void setNextChain(Chain nextChain) {

        nextInChain = nextChain;

    }

    public String validate(Account request, String message) {

        if (request.getActionWanted().equals("value")) {
            return "value";
        } else {
            throw new RuntimeException("something went wrong");
        }
    }
}

package designPatterns.facade;

import designPatterns.facade.model.PayWithCard;
import designPatterns.facade.model.PayWithCheck;
import designPatterns.facade.model.PayWithPhone;

public class PaymentProcessor {

    private PayWithCard payWithCard;
    private PayWithCheck payWithCheck;
    private PayWithPhone payWithPhone;


    public PaymentProcessor() {
        this.payWithCard = new PayWithCard();
        this.payWithCheck = new PayWithCheck();
        this.payWithPhone = new PayWithPhone();
    }

    public String setPayWithCard() {
        return payWithCard.pay();
    }

    public String setPayWithCheck() {
        return payWithCheck.pay();
    }

    public String setPayWithPhone() {
        return payWithPhone.pay();
    }
}

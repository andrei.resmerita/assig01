package designPatterns.facade.contract;

public interface Pay {

    String pay();

}

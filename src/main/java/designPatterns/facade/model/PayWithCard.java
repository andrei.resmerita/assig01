package designPatterns.facade.model;

import designPatterns.facade.contract.Pay;

public class PayWithCard implements Pay {
    @Override
    public String pay() {
        return "Card";
    }
}

package designPatterns.facade.model;

import designPatterns.facade.contract.Pay;

public class PayWithCheck implements Pay {
    @Override
    public String pay() {
        return "Check";
    }
}

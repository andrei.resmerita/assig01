package designPatterns.facade.model;

import designPatterns.facade.contract.Pay;

public class PayWithPhone implements Pay {
    @Override
    public String pay() {
        return "phone";
    }
}

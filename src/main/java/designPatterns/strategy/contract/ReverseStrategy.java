package designPatterns.strategy.contract;

public interface ReverseStrategy {

    String reverse(String text, String word);
}

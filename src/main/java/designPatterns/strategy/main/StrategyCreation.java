package designPatterns.strategy.main;

import designPatterns.strategy.contract.ReverseStrategy;

public class StrategyCreation {

    private ReverseStrategy reverseStrategy;

    public StrategyCreation(ReverseStrategy reverseStrategy) {
        this.reverseStrategy = reverseStrategy;
    }

    public String executeStrategy(String text, String word) {
        return reverseStrategy.reverse(text, word);
    }

}

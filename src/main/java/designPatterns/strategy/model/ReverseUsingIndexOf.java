package designPatterns.strategy.model;

import designPatterns.strategy.contract.ReverseStrategy;

public class ReverseUsingIndexOf implements ReverseStrategy {
    @Override
    public String reverse(String text, String word) {
        if (word == null || text == null || word.isBlank() || text.isBlank()) {
            return null;
        }
        int newIndex;
        int start = 0;
        StringBuilder stringBuilder = new StringBuilder(word);
        String reversedWord = stringBuilder.reverse().toString();
        StringBuilder result = new StringBuilder(text.substring(0, start));
        while ((newIndex = text.indexOf(word, start)) != -1) {
            result.append(reversedWord);
            start = newIndex + word.length();
        }
        result.append(text.substring(start));
        return result.toString();
    }
}

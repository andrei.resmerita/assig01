package designPatterns.strategy.model;

import designPatterns.strategy.contract.ReverseStrategy;

public class ReverseUsingReplace implements ReverseStrategy {
    @Override
    public String reverse(String text, String word) {
        if (word == null || text == null || word.isBlank() || text.isBlank()) {
            return null;
        }
        StringBuilder stringBuilder = new StringBuilder(word);
        String reversedWord = stringBuilder.reverse().toString();
        int start = 0;
        StringBuilder result = new StringBuilder(text.substring(0, start));
        char[] in = word.toCharArray();
        int begin = 0;
        int endW = in.length - 1;
        char temp;
        do {
            temp = in[begin];
            in[begin] = in[endW];
            in[endW] = temp;
            endW--;
            begin++;
        }
        while (endW > begin);
        result.append(text.substring(start));
        result.replace(0, 5, reversedWord);
        return result.toString();
    }
}

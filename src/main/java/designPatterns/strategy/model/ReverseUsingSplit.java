package designPatterns.strategy.model;

import designPatterns.strategy.contract.ReverseStrategy;

public class ReverseUsingSplit implements ReverseStrategy {
    @Override
    public String reverse(String text, String word) {
        if (word == null || text == null) {
            return null;
        }
        if (word.isBlank() || text.isBlank()) {
            return null;
        }
        StringBuilder stringBuilder = new StringBuilder();
        String[] words = text.split(" ");
        for (String s : words) {
            if (s.equalsIgnoreCase(word)) {
                s = new StringBuilder(s).reverse().toString();

            }
            stringBuilder.append(s).append(" ");
        }
        return stringBuilder.toString().trim();
    }
}

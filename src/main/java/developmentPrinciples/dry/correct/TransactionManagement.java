package developmentPrinciples.dry.correct;


import developmentPrinciples.dry.correct.model.Account;
import developmentPrinciples.dry.correct.model.Transaction;

public class TransactionManagement {


    public int getTotalTransactionValue(Account account) {
        int res = 0;
        for (Transaction t1 : account.getTransactionList()) {
            res += t1.getValue();
        }
        return res;
    }

    public double getAverageAmountTransactionValues(Account account) {
        return getTotalTransactionValue(account) / account.getTransactionList().size();
    }

}

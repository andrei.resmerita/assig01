package developmentPrinciples.dry.correct.model;

public class Transaction {
    private int value;

    public Transaction(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}

package developmentPrinciples.dry.incorrect;


import developmentPrinciples.dry.incorrect.model.Account;
import developmentPrinciples.dry.incorrect.model.Transaction;

public class TransactionManagement {


    public int getTotalTransactionValue(Account account) {
        int res = 0;
        for (Transaction t1 : account.getTransactionList()) {
            res += t1.getValue();
        }
        return res;
    }

    public double getAverageAmountTransactionValues(Account account) {
        int res = 0;
        for (Transaction t1 : account.getTransactionList()) {
            res += t1.getValue();
        }
        return res / account.getTransactionList().size();
    }

}

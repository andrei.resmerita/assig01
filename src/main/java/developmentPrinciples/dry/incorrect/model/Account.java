package developmentPrinciples.dry.incorrect.model;

import java.util.List;

public class Account {
    private List<Transaction> transactionList;

    public Account(List<Transaction> transactionList) {
        this.transactionList = transactionList;
    }

    public List<Transaction> getTransactionList() {
        return transactionList;
    }
}

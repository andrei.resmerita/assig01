package developmentPrinciples.kiss.correct;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;


public class JsonParser {

    public Person parse(File file) throws IOException {

        return new ObjectMapper().readValue(file, Person.class);

    }

}

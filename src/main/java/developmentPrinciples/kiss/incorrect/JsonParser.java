package developmentPrinciples.kiss.incorrect;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonToken;

import java.io.File;
import java.io.IOException;

public class JsonParser {


    public Person readFromDictionary(File file, Person person) throws IOException {
        com.fasterxml.jackson.core.JsonParser jsonParser = new JsonFactory().createJsonParser(file);
        while (jsonParser.nextToken() != JsonToken.END_OBJECT) {
            String name = jsonParser.getCurrentName();
            if ("name".equals(name)) {
                jsonParser.nextToken();
                person.setName(jsonParser.getText());
            }
            if ("address".equals(name)) {
                person.setAddress(jsonParser.getText());
            }

        }
        return person;
    }

}

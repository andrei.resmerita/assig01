package developmentPrinciples.lawOfDemeter.correct;

public class Address {

    private String city;

    public Address(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }
}

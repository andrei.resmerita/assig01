package developmentPrinciples.lawOfDemeter.correct;

public class GetCityOfClient {

    public String getCityOfClient(Client client) {
        return client.getCity();
    }

}

package developmentPrinciples.lawOfDemeter.incorrect;

public class Address {

    private String city;
    private Country country;

    public Address(String city, Country country) {
        this.city = city;
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public Country getCountry() {
        return country;
    }
}

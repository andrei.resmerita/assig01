package developmentPrinciples.lawOfDemeter.incorrect;

public class Country {
    private String name;

    public Country(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

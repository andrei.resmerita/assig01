package developmentPrinciples.solid.dependencyInversion.correct.dao;

import developmentPrinciples.solid.dependencyInversion.correct.model.Customer;

public interface CustomerDAO {

    Customer addCustomer(String name);

}

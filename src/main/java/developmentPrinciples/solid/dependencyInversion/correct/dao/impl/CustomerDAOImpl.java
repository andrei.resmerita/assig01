package developmentPrinciples.solid.dependencyInversion.correct.dao.impl;

import developmentPrinciples.solid.dependencyInversion.correct.dao.CustomerDAO;
import developmentPrinciples.solid.dependencyInversion.correct.model.Customer;

public class CustomerDAOImpl implements CustomerDAO {
    @Override
    public Customer addCustomer(String name) {
        return new Customer(name);
    }
}

package developmentPrinciples.solid.dependencyInversion.correct.service;

import developmentPrinciples.solid.dependencyInversion.correct.model.Customer;

public interface CustomerService {

    Customer addCustomer(String name);

}

package developmentPrinciples.solid.dependencyInversion.correct.service.impl;

import developmentPrinciples.solid.dependencyInversion.correct.dao.CustomerDAO;
import developmentPrinciples.solid.dependencyInversion.correct.model.Customer;
import developmentPrinciples.solid.dependencyInversion.correct.service.CustomerService;

public class CustomerServiceImpl implements CustomerService {

    private CustomerDAO customerDAO;

    @Override
    public Customer addCustomer(String name) {
        return customerDAO.addCustomer(name);
    }
}

package developmentPrinciples.solid.dependencyInversion.incorect.dao;

import developmentPrinciples.solid.dependencyInversion.incorect.model.Customer;

public interface CustomerDAO {

    Customer addCustomer(String name);

}

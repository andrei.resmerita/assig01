package developmentPrinciples.solid.dependencyInversion.incorect.dao.impl;

import developmentPrinciples.solid.dependencyInversion.incorect.dao.CustomerDAO;
import developmentPrinciples.solid.dependencyInversion.incorect.model.Customer;

public class CustomerDAOImpl implements CustomerDAO {
    @Override
    public Customer addCustomer(String name) {
        return new Customer(name);
    }
}

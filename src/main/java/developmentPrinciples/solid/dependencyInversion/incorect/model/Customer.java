package developmentPrinciples.solid.dependencyInversion.incorect.model;

import java.util.Objects;

public class Customer {

    private String id;
    private String name;
    private String country;

    public Customer(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCountry() {
        return country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return Objects.equals(id, customer.id) && Objects.equals(name, customer.name) && Objects.equals(country, customer.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, country);
    }
}

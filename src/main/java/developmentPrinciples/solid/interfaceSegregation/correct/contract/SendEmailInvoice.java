package developmentPrinciples.solid.interfaceSegregation.correct.contract;

public interface SendEmailInvoice {

    String sendEmailInvoice();

}

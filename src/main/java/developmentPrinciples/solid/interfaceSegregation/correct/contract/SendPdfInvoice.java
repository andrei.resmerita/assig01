package developmentPrinciples.solid.interfaceSegregation.correct.contract;

public interface SendPdfInvoice {

    String sendPdfInvoice();
}

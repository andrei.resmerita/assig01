package developmentPrinciples.solid.interfaceSegregation.correct.impl;

import developmentPrinciples.solid.interfaceSegregation.correct.contract.SendEmailInvoice;

public class SendEmailInvoiceImpl implements SendEmailInvoice {
    @Override
    public String sendEmailInvoice() {
        return "email";
    }
}

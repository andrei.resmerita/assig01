package developmentPrinciples.solid.interfaceSegregation.correct.impl;

import developmentPrinciples.solid.interfaceSegregation.correct.contract.SendPdfInvoice;

public class SendPdfInvoiceImpl implements SendPdfInvoice {
    @Override
    public String sendPdfInvoice() {
        return "pdf";
    }
}

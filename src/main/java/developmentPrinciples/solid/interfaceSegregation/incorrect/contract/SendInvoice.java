package developmentPrinciples.solid.interfaceSegregation.incorrect.contract;

public interface SendInvoice {

    String sendPdfInvoice();

    String sendEmailInvoice();
}

package developmentPrinciples.solid.interfaceSegregation.incorrect.contract;

public class SendInvoiceImpl implements SendInvoice {


    //now we must implement both methods either if we want only to send email invoices
    @Override
    public String sendPdfInvoice() {
        return null;
    }

    @Override
    public String sendEmailInvoice() {
        return null;
    }
}

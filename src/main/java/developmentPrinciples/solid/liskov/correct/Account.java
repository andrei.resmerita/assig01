package developmentPrinciples.solid.liskov.correct;


public abstract class Account {


    public abstract int deposit(int value);

}
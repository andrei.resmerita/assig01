package developmentPrinciples.solid.liskov.correct;


public class DepositAccount extends Account {

    private int balance;

    public DepositAccount(int balance) {
        this.balance = balance;
    }

    public int getBalance() {
        return balance;
    }

    @Override
    public int deposit(int value) {
        balance = value + balance;
        return balance;
    }
}

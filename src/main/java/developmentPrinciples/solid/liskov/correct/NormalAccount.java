package developmentPrinciples.solid.liskov.correct;


public class NormalAccount extends WithdrawableAccount {

    private int balance;

    public NormalAccount(int balance) {
        this.balance = balance;
    }

    public int getBalance() {
        return balance;
    }


    @Override
    public int deposit(int value) {
        return balance += value;
    }

    @Override
    public int withdraw(int value) {
        return balance -= value;
    }
}
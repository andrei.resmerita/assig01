package developmentPrinciples.solid.liskov.correct;

public abstract class WithdrawableAccount extends Account {

    public abstract int withdraw(int value);
}

package developmentPrinciples.solid.liskov.incorrect;


public abstract class Account {


     public abstract int deposit(int value);

     public abstract int withdraw(int value);
}
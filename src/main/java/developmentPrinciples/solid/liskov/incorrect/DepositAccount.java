package developmentPrinciples.solid.liskov.incorrect;


public class DepositAccount extends Account {

    private int balance;

    public DepositAccount(int balance) {
        this.balance = balance;
    }

    public int getBalance() {
        return balance;
    }

    @Override
    public int deposit(int value) {
        balance = value + balance;
        return balance;
    }

    @Override
    public int withdraw(int value) {
        throw new RuntimeException("cant withdraw from this account");
    }
}

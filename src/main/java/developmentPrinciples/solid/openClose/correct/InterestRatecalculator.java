package developmentPrinciples.solid.openClose.correct;

public abstract class InterestRatecalculator {
    private int value;
    private int interestRate;

    public abstract int calculate(int value);

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(int interestRate) {
        this.interestRate = interestRate;
    }
}

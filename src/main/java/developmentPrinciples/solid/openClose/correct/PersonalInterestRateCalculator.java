package developmentPrinciples.solid.openClose.correct;

public class PersonalInterestRateCalculator extends InterestRatecalculator {
    private int anualBonus;

    public PersonalInterestRateCalculator(int anualBonus) {
        this.anualBonus = anualBonus;
    }

    @Override
    public int calculate(int value) {
        return getValue() * getInterestRate() - anualBonus;
    }

    public void addEveryYearToBonus() {
        anualBonus = anualBonus + 5;
    }

}

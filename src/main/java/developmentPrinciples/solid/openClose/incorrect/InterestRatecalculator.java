package developmentPrinciples.solid.openClose.incorrect;

public abstract class InterestRatecalculator {
    private int value;
    private int interestRate;
    private int annualBonnus;

    public InterestRatecalculator(int value, int interestRate, int annualBonnus) {
        this.value = value;
        this.interestRate = interestRate;
        this.annualBonnus = annualBonnus;
    }

    public int calculatePersonalRate() {
        return value * interestRate - annualBonnus;
    }
}

package developmentPrinciples.solid.singleResponsability.correct;

import java.util.Objects;

public class Account {

    private Long id;
    private String accountNumber;
    private String accountOwner;

    public Account(Long id, String accountNumber, String accountOwner) {
        this.id = id;
        this.accountNumber = accountNumber;
        this.accountOwner = accountOwner;
    }

    public Long getId() {
        return id;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getAccountOwner() {
        return accountOwner;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public void setAccountOwner(String accountOwner) {
        this.accountOwner = accountOwner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Objects.equals(id, account.id) && Objects.equals(accountNumber, account.accountNumber) && Objects.equals(accountOwner, account.accountOwner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, accountNumber, accountOwner);
    }
}

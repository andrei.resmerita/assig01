package developmentPrinciples.solid.singleResponsability.correct;

public class ModifyAccountOwner {


    public Account modifyAccountNumber(Account account, String name) {
        account.setAccountOwner(name);
        return account;
    }

}

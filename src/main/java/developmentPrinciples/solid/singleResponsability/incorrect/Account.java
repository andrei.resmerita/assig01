package developmentPrinciples.solid.singleResponsability.incorrect;

import java.util.List;

public class Account {

    private Long id;
    private String accountNumber;
    private String accountOwner;
    private Person person;
    private List<Transaction> transactionList;

    public Account(Long id, String accountNumber, String accountOwner, Person person, List<Transaction> transactionList) {
        this.id = id;
        this.accountNumber = accountNumber;
        this.accountOwner = accountOwner;
        this.person = person;
        this.transactionList = transactionList;
    }

    public Long getId() {
        return id;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getAccountOwner() {
        return accountOwner;
    }

    public Person getPerson() {
        return person;
    }

    public List<Transaction> getTransactionList() {
        return transactionList;
    }

    private long calculateAmountOfTransaction(Account account) {
        long res = 0;
        for (Transaction t : account.getTransactionList()) {
            res += t.getValue();
        }
        return res;
    }
}

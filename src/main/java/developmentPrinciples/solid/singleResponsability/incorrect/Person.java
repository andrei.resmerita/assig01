package developmentPrinciples.solid.singleResponsability.incorrect;

import java.util.List;

public class Person {
    Long id;
    private String name;
    private Integer age;
    private String hairColor;
    private Double height;
    private List<Account> accountList;

    public Person(Long id, String name, Integer age, String hairColor, Double height, List<Account> accountList) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.hairColor = hairColor;
        this.height = height;
        this.accountList = accountList;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }

    public String getHairColor() {
        return hairColor;
    }

    public Double getHeight() {
        return height;
    }

    public List<Account> getAccountList() {
        return accountList;
    }

    public int getTotalAccounts(Person p) {
        return p.getAccountList().size();
    }

    public void cancelAllTransactions(Person p) {
        for (Account a : p.getAccountList()) {
            for (Transaction t : a.getTransactionList()) {
                t.setValue(0);
            }
        }
    }
}

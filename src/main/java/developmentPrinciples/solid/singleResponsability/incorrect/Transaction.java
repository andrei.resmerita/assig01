package developmentPrinciples.solid.singleResponsability.incorrect;

public class Transaction {

    private long id;
    private long value;

    public Transaction(long id, long value) {
        this.id = id;
        this.value = value;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }
}

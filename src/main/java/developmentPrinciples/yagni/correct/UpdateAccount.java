package developmentPrinciples.yagni.correct;

import developmentPrinciples.yagni.correct.model.Account;

public class UpdateAccount {

    public Account updateAccount(Account account, String name) {
        account.setName(name);
        return account;
    }

}

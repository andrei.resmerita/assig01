package developmentPrinciples.yagni.incorrect;

import developmentPrinciples.yagni.correct.model.Account;

public class UpdateAccount {

    public Account updateAccount(Account account, String name) {
        if (account == null) {
            createAcc(name);
        }
        account.setName(name);
        return account;
    }

    public Account createAcc(String name) {
        return new Account(name);
    }

}

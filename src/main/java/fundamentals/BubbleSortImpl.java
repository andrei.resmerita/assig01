package fundamentals;

import java.util.Arrays;

public class BubbleSortImpl {
    public int[] bubbleSort(int[] arr) {

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
        return arr;

    }

    public static void main(String[] args) {
        int[] arr = new int[]{10, 15, 20, 5, 4, 3, 6, 1};
        System.out.println(Arrays.toString(new BubbleSortImpl().bubbleSort(arr)));
    }
}

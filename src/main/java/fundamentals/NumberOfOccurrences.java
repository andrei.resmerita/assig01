package fundamentals;

public class NumberOfOccurrences {


    public int numberOfOccurrences(int[] arr, int number) {
        if (arr == null) {
            return 0;
        }
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == number)
                count++;
        }
        return count;
    }

    public int numberOfOccurrences(String[] arr, String word) {
        if (arr == null) {
            return 0;
        }
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].equals(word))
                count++;
        }
        return count;
    }

    public static void main(String[] args) {
        int[] arr1 = new int[]{1, 2, 1, 3, 1, 4, 1};
        String[] arr2 = new String[]{"Word", "Song", "Toy", "Word"};
        System.out.println(new NumberOfOccurrences().numberOfOccurrences(arr1, 1));
        System.out.println(new NumberOfOccurrences().numberOfOccurrences(arr2, "Word"));
    }

}

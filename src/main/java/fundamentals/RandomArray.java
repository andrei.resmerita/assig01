package fundamentals;

import java.util.Arrays;
import java.util.Random;

public class RandomArray {
    public static void main(String[] args) {
        Random random = new Random();
        int[] arr = new int[5];
        for (int i = 0; i < arr.length; i++) {
            arr[i]= random.nextInt();
        }
        
        System.out.println(Arrays.toString(arr));
    }
}

package fundamentals;

import java.util.Random;

public class RandomArraySearch {
    public int getRandomNumber(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min) + min;
    }

    public int[] generateRandomArray(int length) {
        int[] arr = new int[length];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = new RandomArraySearch().getRandomNumber(1, 10);
        }
        return arr;
    }

    public int findNumber(int[] arr, int n) {
        if (arr == null) {
            throw new NullPointerException();
        }
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == n) return n;
        }
        return -1;
    }

    public static void main(String[] args) {
        RandomArraySearch randomArraySearch = new RandomArraySearch();
        int[] arr = randomArraySearch.generateRandomArray(5);
        int number = 2;
        System.out.println(randomArraySearch.findNumber(arr, number));
    }
}

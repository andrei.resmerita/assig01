package fundamentals;

import java.util.Arrays;
import java.util.Scanner;

public class ReadArrFromKeyboard {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter length of array");
        int length = scanner.nextInt();
        String[] arr = new String[length];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = scanner.next();
        }
        System.out.println(Arrays.toString(arr));
    }

}

package fundamentals;

import java.util.Arrays;

public class ReverseArray {
    public int[] reverseArray(int[] arr) {
        if(arr==null){
            return null;
        }
        int size= arr.length-1;
        int[] newArr = new int[arr.length];
        for(int i = 0;i< arr.length;i++){
            newArr[i]=arr[size--];
        }
        return newArr;
    }

    public static void main(String[] args) {
        int[] arr = new int[]{1, 2, 3, 4, 5};
        System.out.println(Arrays.toString(new ReverseArray().reverseArray(arr)));
    }
}

package genericsAndCollections.dictionary;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReadFromDictionary {

    public Map<String, List<String>> readFromJson(File file) throws IOException {

        return new ObjectMapper().readValue(file, HashMap.class);

    }

}

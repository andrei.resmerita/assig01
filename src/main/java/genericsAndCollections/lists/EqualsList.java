package genericsAndCollections.lists;


import java.util.HashSet;
import java.util.List;

public class EqualsList {

    public static <T> boolean areEquals(List<T> list1, List<T> list2) {
        return new HashSet<>(list1).equals(new HashSet<>(list2));
    }

}

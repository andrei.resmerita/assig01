package genericsAndCollections.queue;


public class Queue<T> {

    private int front;
    private int rear;
    private int size;
    private T[] arr;


    public Queue() {
        this.front = 0;
        this.rear = -1;
        this.size = 0;
        this.arr = (T[]) new Object[size];

    }

    public boolean add(T data) {
        T[] newArr = (T[]) new Object[size + 1];
        if (size >= 0) {
            System.arraycopy(arr, 0, newArr, 0, size);
        }
        if (rear == size - 1) {
            rear = 0;
        }
        arr = newArr;
        arr[rear] = data;
        rear++;
        size++;
        return true;
    }


    public T remove() {
        Object obj = null;
        if (isEmpty()) {
            throw new RuntimeException("Queue is empty");
        } else {
            front++;
            if (front == size) {
                obj = arr[front - 1];
                front = 0;
            } else {
                obj = arr[front - 1];
            }
            size--;
        }
        return (T) obj;
    }

    public T get() {
        if (isEmpty()) {
            throw new RuntimeException("Queue is empty!");
        }
        return arr[front];
    }

    public T[] list(Queue<T> queue) {
        if (isEmpty()) {
            throw new RuntimeException("Queue is empty");
        }
        T[] elements = (T[]) new Object[queue.size];
        if (queue.size >= 0) System.arraycopy(arr, 0, elements, 0, queue.size);
        return elements;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int getFront() {
        return front;
    }

    public int getRear() {
        return rear;
    }

    public int getSize() {
        return size;
    }

    public T[] getArr() {
        return arr;
    }

}

package genericsAndCollections.sortUsers.sortUserArray;

import genericsAndCollections.sortUsers.model.User;

import java.util.Collections;
import java.util.List;

public class SortUserArray {

    public List<User> sortUsersByAge(List<User> list) {
        Collections.sort(list);
        return list;
    }

}

package genericsAndCollections.stack;

import java.util.Arrays;

public class Stack<T> {

    private int size;
    private T[] arr;
    private int top;


    public Stack() {
        this.size = 0;
        this.arr = (T[]) new Object[size];
        this.top = -1;
    }

    public void add(T pushedElement) {
        T[] newArr = (T[]) new Object[size + 1];
        if (size >= 0) {
            System.arraycopy(arr, 0, newArr, 0, size);
        }
        size++;
        arr = newArr;
        top++;
        arr[top] = pushedElement;
    }

    public T remove() {
        if (!isEmpty()) {
            int returnedTop = top;
            T result = arr[top];
            arr[top] = null;
            top--;
            return result;
        } else {
            throw new RuntimeException("Stack is empty");
        }
    }

    public T get() {
        if (this.isEmpty())
            throw new RuntimeException("Stack is empty");
        else {
            return arr[top];
        }
    }


    public T[] list(Stack<T> stack) {
        if (isEmpty()) {
            throw new RuntimeException("Stack is empty");
        }
        T[] elements = (T[]) new Object[stack.size];
        if (stack.size >= 0) System.arraycopy(stack.arr, 0, elements, 0, stack.size);
        return elements;
    }

    public boolean isEmpty() {
        return (top == -1);
    }

    public boolean isFull() {
        return (size - 1 == top);
    }

    public int getSize() {
        return size;
    }

    public T[] getArr() {
        return arr;
    }


    @Override
    public String toString() {
        return "Stack{" +
                "size=" + size +
                ", arr=" + Arrays.toString(arr) +
                ", top=" + top +
                '}';
    }
}

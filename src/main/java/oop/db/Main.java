package oop.db;

import oop.db.dao.PersonDAO;
import oop.db.entity.Entity;
import oop.db.dao_impl.MySqlDaoImpl;
import oop.db.dao_impl.OracleDaoImpl;
import oop.db.dao_impl.SQLiteImpl;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Type in database name");
        Scanner scanner = new Scanner(System.in);
        String db = scanner.next();
        PersonDAO personDAO;
        if (db.equalsIgnoreCase("mysql")) {
            personDAO = new MySqlDaoImpl();
        }
        if (db.equalsIgnoreCase("oracle")) {
            personDAO = new OracleDaoImpl();
        } else {
            personDAO = new SQLiteImpl();
        }
        personDAO.createPerson(new Entity("profesor"));
    }
}

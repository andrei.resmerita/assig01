package oop.db.dao;


import oop.db.entity.Entity;

public interface PersonDAO {


    void createPerson(Entity entity);

    void readPerson(Entity entity);

    void updatePerson(Entity entity);

    void deletePerson(Entity entity);

}

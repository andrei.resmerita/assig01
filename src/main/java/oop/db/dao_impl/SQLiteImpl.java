package oop.db.dao_impl;


import oop.db.entity.Entity;
import oop.db.dao.PersonDAO;

public class SQLiteImpl implements PersonDAO {
    @Override
    public void createPerson(Entity entity) {
        System.out.println("Creating a " + entity.getType() + " in SQLite");
    }

    @Override
    public void readPerson(Entity entity) {
        System.out.println("Reading a " + entity.getType() + " in SQLite");
    }

    @Override
    public void updatePerson(Entity entity) {
        System.out.println("Updating a " + entity.getType() + " in SQLite");
    }

    @Override
    public void deletePerson(Entity entity) {
        System.out.println("Deleting a " + entity.getType() + " in SQLite");
    }
}

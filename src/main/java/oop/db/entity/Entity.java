package oop.db.entity;

public class Entity {
    private long id;
    private String type;

    public Entity(String type) {
        this.type = type;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
package oop.persons;

import oop.persons.model.Janitor;
import oop.persons.model.Professor;
import oop.persons.model.SecurityGuard;
import oop.persons.model.Student;

public class Main {
        public static void main(String[] args) {
            Professor theProfessor = new Professor(1, "Ion", 55, 3);
            theProfessor.talk();
            theProfessor.dance();
            theProfessor.teach();


            Student theStudent = new Student(21, "Andrei", 27, 3);
            theStudent.talk();
            theStudent.dance();
            theStudent.learn();

            Janitor theJanitor = new Janitor(44, "Nutu", 43, 3);
            theJanitor.talk();
            theJanitor.dance();
            theJanitor.openGate();

            SecurityGuard theSecurityGuard = new SecurityGuard(55, "Robert", 23, 100);
            theSecurityGuard.talk();
            theSecurityGuard.dance();
            theSecurityGuard.verifyIdCard();

        }
}

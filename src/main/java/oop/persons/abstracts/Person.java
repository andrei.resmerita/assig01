package oop.persons.abstracts;

public abstract class Person {
    public int id;
    public String name;
    public int age;

    public Person(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }


    public  void talk(){
        System.out.println("A person is talking");};

    public abstract void dance();
}

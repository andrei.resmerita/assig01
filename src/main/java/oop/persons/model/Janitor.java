package oop.persons.model;

import oop.persons.abstracts.Person;

public class Janitor extends Person {

    int keys;

    public Janitor(int id, String name, int age, int keys) {
        super(id, name, age);
        this.keys = keys;
    }

    @Override
    public void dance() {
        System.out.println("Dancing like a janitor");
    }

    public void openGate() {
        System.out.println("I am opening the gate");
    }
}

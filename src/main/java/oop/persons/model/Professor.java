package oop.persons.model;

import oop.persons.abstracts.Person;

public class Professor extends Person {

    int testsToCorrect;

    public Professor(int id, String name, int age) {
        super(id, name, age);
    }

    public Professor(int id, String name, int age, int testsToCorrect) {
        super(id, name, age);
        this.testsToCorrect = testsToCorrect;
    }

    @Override
    public void dance() {
        System.out.println("Dancing as a professor");
    }

    public void teach() {
        System.out.println("I am teaching");
    }
}
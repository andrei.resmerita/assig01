package oop.persons.model;

import oop.persons.abstracts.Person;

public class SecurityGuard extends Person {

    int ammunition;

    public SecurityGuard(int id, String name, int age, int ammunition) {
        super(id, name, age);
        this.ammunition = ammunition;
    }

    @Override
    public void dance() {
        System.out.println("Dancing as a security guard");
    }


    public void verifyIdCard() {
        System.out.println("Please show me your id"
        );
    }

}

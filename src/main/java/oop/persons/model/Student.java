package oop.persons.model;

import oop.persons.abstracts.Person;

public class Student extends Person {

    int positionInClassroom;

    public Student(int id, String name, int age, int positionInClassroom) {
        super(id, name, age);
        this.positionInClassroom = positionInClassroom;
    }


    @Override
    public void dance() {
        System.out.println("Dancing like a student");
    }

    public void learn() {
        System.out.println("I am learning");
    }
}

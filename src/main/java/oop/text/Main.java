package oop.text;

public class Main {
    public static void main(String[] args) {
        String word = "Lorem";
        String text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas congue.";
        System.out.println(
                new Text().reverse(word, text, 0, text.length() - 1));
        System.out.println(new Text().reverse(word,text));
        System.out.println(new Text().reverse(word, text, 0));

    }

}

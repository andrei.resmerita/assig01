package oop.text;

public class Text {
    public String reverse(String word, String text) {

        if (word == null || text == null) {
            return null;
        }
        if (word.isBlank()||text.isBlank()){
            return null;
        }
        StringBuilder stringBuilder = new StringBuilder();
        String[] words = text.split(" ");
        for (String s : words) {
            if (s.equalsIgnoreCase(word)) {
                s= new StringBuilder(s).reverse().toString();

            }
            stringBuilder.append(s).append(" ");
        }
        return stringBuilder.toString().trim();
    }

    public String reverse(String word, String text, int start) {
        if (word == null || text == null ||word.isBlank()||text.isBlank()||start!=0) {
            return null;
        }
        int newIndex;
        StringBuilder stringBuilder = new StringBuilder(word);
        String reversedWord = stringBuilder.reverse().toString();
        StringBuilder result = new StringBuilder(text.substring(0, start));
        while ((newIndex = text.indexOf(word, start)) != -1) {
            result.append(reversedWord);
            start = newIndex + word.length();
        }
        result.append(text.substring(start));
        return result.toString();

    }

    public String reverse(String word, String text, int start, int end) {
        if (word == null || text == null || word.isBlank()||text.isBlank()||start!=0||end!=text
        .length()-1) {
            return null;
        }
        StringBuilder stringBuilder = new StringBuilder(word);
        String reversedWord = stringBuilder.reverse().toString();

        StringBuilder result = new StringBuilder(text.substring(0, start));
        char[] in = word.toCharArray();
        int begin = 0;
        int endW = in.length - 1;
        char temp;
        do {
            temp = in[begin];
            in[begin] = in[endW];
            in[endW] = temp;
            endW--;
            begin++;
        }
        while (endW > begin);
        result.append(text.substring(start));
        result.replace(0, 5, reversedWord);
        return result.toString();
    }


}

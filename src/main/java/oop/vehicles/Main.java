package oop.vehicles;

import oop.vehicles.interfaces.OpenTrunk;
import oop.vehicles.interfaces.SetSail;
import oop.vehicles.models.Boat;
import oop.vehicles.models.Car;
import oop.vehicles.models.Vehicle;

public class Main {


    public static void main(String[] args) {
        String color = "red";
        String nameOfCar = "opel";
        int numberOfDoors = 2;
        String nameOfBoat = "UMS";
        int numberOfEngines = 2;
        Vehicle car = new Car(nameOfCar, color, numberOfDoors);
        car.start();
        car.stop();

        System.out.println("///////");

        Vehicle boat = new Boat(nameOfBoat, color, numberOfEngines);
        boat.start();
        boat.stop();

        System.out.println("/////");

        OpenTrunk openTrunk = new Car(nameOfCar, color, numberOfDoors);
        openTrunk.openTrunk();
        SetSail setSail = new Boat(nameOfBoat, color, numberOfEngines);
        setSail.setSails();

    }

}

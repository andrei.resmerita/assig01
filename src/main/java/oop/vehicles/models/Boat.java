package oop.vehicles.models;

import oop.vehicles.interfaces.SetSail;
public class Boat extends Vehicle implements SetSail {
    private int numberOfEngines;


    public Boat(int numberOfEngines) {
        this.numberOfEngines = numberOfEngines;
    }

    public Boat(String name, String color, int numberOfEngines) {
        super(name, color);
        this.numberOfEngines = numberOfEngines;
    }

    public int getNumberOfEngines() {
        return numberOfEngines;
    }

    public void setNumberOfEngines(int numberOfEngines) {
        this.numberOfEngines = numberOfEngines;
    }


    @Override
    public void start() {
        System.out.println("Starting engine of the boat");
    }

    @Override
    public void stop() {
        System.out.println("Stopping boat");
    }


    @Override
    public void setSails() {
        System.out.println("the journey begin");
    }
}

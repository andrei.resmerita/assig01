package oop.vehicles.models;

import oop.vehicles.interfaces.OpenTrunk;

public class Car extends Vehicle implements OpenTrunk {

    private int numberOfDoors;

    public Car(int numberOfDoors) {
        this.numberOfDoors = numberOfDoors;
    }

    public Car(String name, String color, int numberOfDoors) {
        super(name, color);
        this.numberOfDoors = numberOfDoors;
    }

    @Override
    public void start() {
        System.out.println("Starting engine of the car");
    }

    @Override
    public void stop() {
        System.out.println("Stopping car");
    }


    @Override
    public void openTrunk() {
        System.out.println("Opening trunk");
    }
}


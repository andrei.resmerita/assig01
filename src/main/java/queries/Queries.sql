 -- assign01  Alter all tables and add creation_dateand created_bycolumns
 -- add creation_date
Alter TABLE sbs_albums
ADD creation_date date,
ADD created_by varchar(50);

Alter TABLE sbs_artist_genres
ADD creation_date date,
ADD created_by varchar(50);

Alter TABLE sbs_artists
ADD creation_date date,
ADD created_by varchar(50);

Alter TABLE sbs_genres
ADD creation_date date,
ADD created_by varchar(50);

-- Add a text column to SBS_ALBUMS that represents sold_amount
Alter TABLE sbs_albums
ADD sold_amount text;


-- Insert at least 1 value in each table, but preferably: 2 artists, 4 genres, 
-- assign genres to artists making sure each artist hasat least 2 genres, 2 albums for each artist

insert into sbs_artists (name, origin, details, launch_date)
	VALUES ('Madonna', 'USA', 'Still Active', STR_TO_DATE('01/01/2002','%d/%m/%Y'));
    
    insert into sbs_artists (name, origin, details, launch_date)
	VALUES ('Michael Jackson', 'USA', 'Still Active', STR_TO_DATE('01/01/1095','%d/%m/%Y'));
    
    INSERT INTO my_database.sbs_genres(
	 name, description, artist_origin, cultural_origin, first_appeared_in)
	VALUES ( 'Pop', 'Pop is Life', 
			'Madonna', 'Usa', STR_TO_DATE('01/01/1969','%d/%m/%Y'));
            
            INSERT INTO my_database.sbs_genres(
	 name, description, artist_origin, cultural_origin, first_appeared_in)
	VALUES ( 'Electro', 'Pop is Life', 
			'Madonna', 'Usa', STR_TO_DATE('01/01/1966','%d/%m/%Y'));
            
     INSERT INTO my_database.sbs_artist_genres(
	id_artist, id_genre)
	VALUES ( 
			(SELECT id FROM sbs_artists WHERE name like 'Metallica'), 
			(SELECT id FROM sbs_genres WHERE name like 'Rock and Roll'));
    
    INSERT INTO sbs_artist_genres(
	id_artist, id_genre)
	VALUES (
			(SELECT id FROM sbs_artists WHERE name like 'Madonna'), 
			(SELECT id FROM sbs_genres WHERE name like 'Pop'));
             INSERT INTO sbs_artist_genres(
	id_artist, id_genre)
	VALUES (
			(SELECT id FROM sbs_artists WHERE name like 'Madonna'), 
			(SELECT id FROM sbs_genres WHERE name like 'Electro'));
            
            INSERT INTO my_database.sbs_albums(
	title, details, release_date, id_artist)
	VALUES ('Madona album 1', '', STR_TO_DATE('01/01/1984','%d/%m/%Y'), (SELECT id FROM sbs_artists WHERE name like 'Madonna'));
    
    INSERT INTO my_database.sbs_albums(
	title, details, release_date, id_artist)
	VALUES ('Madonna', '', STR_TO_DATE('01/01/1984','%d/%m/%Y'), (SELECT id FROM sbs_artists WHERE name like 'Madonna'));
    
    -- After you have inserted values for all columns change  the type of sold_amount from text to integer;
    
    ALTER TABLE sbs_albums MODIFY sold_amount INTEGER;
    
    -- Select all artists
    
SELECT 
    *
FROM
    sbs_artists;
    
    -- Select an artist by id
SELECT 
    *
FROM
    sbs_artists a
WHERE
    a.id = 1;
-- Select an artist by id and name

SELECT 
    *
FROM
    sbs_artists a
WHERE
    a.id = 1 AND a.name = 'Metallica';

    -- Select all albums for an artist based on artist id
    
SELECT 
    *
FROM
    sbs_albums
WHERE
    id_artist = 1;
    
   -- Count all albums for an artist based on artist id
   
SELECT 
    COUNT(s.id)
FROM
    sbs_albums s
WHERE
    s.id_artist = 1;
   
   -- Count all genres for an artist based on artist id
   
SELECT 
    COUNT(sg.id)
FROM
    sbs_artist_genres sg
WHERE
    id_artist = 1;
    
    -- Sum all sold amounts for each artist id
    
SELECT 
    SUM(s.sold_amount)
FROM
    sbs_albums s
GROUP BY s.id_artist;

-- Select artists having sales more than 100

SELECT 
    sbs_artists.name, sbs_albums.sold_amount
FROM
    sbs_artists
        LEFT JOIN
    sbs_albums ON sbs_artists.id = sbs_albums.id_artist
WHERE
    sbs_albums.sold_amount > 100;

-- Select artists sorted by album sales

SELECT 
    sbs_artists.name, sbs_albums.sold_amount
FROM
    sbs_artists
        LEFT JOIN
    sbs_albums ON sbs_artists.id = sbs_albums.id_artist
ORDER BY sbs_albums.sold_amount;

-- Select artists with launch_datebetween two given dates

SELECT 
    sbs_artists.name
FROM
    sbs_artists
WHERE
    sbs_artists.launch_date BETWEEN CAST('1990-01-01' AS DATE) AND CAST('2000-01-01' AS DATE);

-- Select artists with launch_datebefore a given date

SELECT 
    sbs_artists.name
FROM
    sbs_artists
WHERE
    sbs_artists.launch_date < CAST('2000-01-01' AS DATE);

-- Using union -Select all artists with albums and all artists that do not have albums

SELECT 
    name
FROM
    sbs_artists 
UNION SELECT 
    sbs_albums.id
FROM
    sbs_albums
WHERE
    sbs_albums.id IS NOT NULL;

SELECT 
    name
FROM
    sbs_artists 
UNION SELECT 
    sbs_albums.id
FROM
    sbs_albums
WHERE
    sbs_albums.id IS NULL;

-- Select all artists for a specific genre based on genre name
SELECT 
    art.name AS `artist name`
FROM
    sbs_artists art
        JOIN
    sbs_artist_genres sag ON art.id = sag.id_artist
        JOIN
    sbs_genres sg ON sg.id = sag.id_genre
WHERE
    sg.name LIKE 'Heavy Metal';
    
  --  Select all albums for an artist based on artist name
SELECT 
    alb.title
FROM
    sbs_albums alb
        LEFT JOIN
    sbs_artists a ON a.id = alb.id_artist
WHERE
    a.name LIKE 'Metallica';
  
  -- Select all albums for each artist
  
SELECT 
    alb.title, a.name
FROM
    sbs_albums alb
        LEFT JOIN
    sbs_artists a ON a.id = alb.id_artist;
  
  -- Count all albums for each artist
   
SELECT 
    a.name, COUNT(alb.id) AS 'number of albums'
FROM
    sbs_albums alb
        JOIN
    sbs_artists a ON a.id = alb.id_artist
GROUP BY a.name;


 -- Count all genres for each artist
SELECT 
    art.name, COUNT(gen.id)
FROM
    sbs_genres gen
        JOIN
    sbs_artist_genres sag ON sag.id_genre = gen.id
        JOIN
    sbs_artists art ON art.id = sag.id_artist
GROUP BY art.name;
 
 
 -- Sum all sold amounts for each artist id
 
SELECT 
    alb.id_artist, SUM(alb.sold_amount) AS 'sum'
FROM
    sbs_albums alb
GROUP BY alb.id_artist;
 
-- Select artists having sales more than 100

SELECT 
    a.name
FROM
    sbs_artists a
        LEFT JOIN
    sbs_albums alb ON a.id = alb.id_artist
WHERE
    alb.sold_amount > 100
GROUP BY a.name;

-- Select artists sorted by album sales
SELECT 
    SUM(alb.sold_amount) AS sum, a.name
FROM
    sbs_albums alb
        JOIN
    sbs_artists a ON a.id = alb.id_artist
GROUP BY alb.id_artist
ORDER BY sum DESC;

-- Select artists, albums and genres with artists launch_datebetween two given dates

SELECT 
    art.name,
    GROUP_CONCAT(DISTINCT gen.name),
    GROUP_CONCAT(DISTINCT alb.title)
FROM
    sbs_artists art
        INNER JOIN
    sbs_albums alb ON art.id = alb.id_artist
        INNER JOIN
    sbs_artist_genres sag ON sag.id_artist = art.id
        INNER JOIN
    sbs_genres gen ON gen.id = sag.id_genre
WHERE
    art.launch_date BETWEEN '1995-01-01' AND '2000-01-01'
GROUP BY art.name;

-- create view for the previous query 

CREATE VIEW data_from_3_tables AS
    SELECT 
        art.name,
        GROUP_CONCAT(DISTINCT gen.name),
        GROUP_CONCAT(DISTINCT alb.title)
    FROM
        sbs_artists art
            INNER JOIN
        sbs_albums alb ON art.id = alb.id_artist
            INNER JOIN
        sbs_artist_genres sag ON sag.id_artist = art.id
            INNER JOIN
        sbs_genres gen ON gen.id = sag.id_genre
    WHERE
        art.launch_date BETWEEN '1995-01-01' AND '2000-01-01'
    GROUP BY art.name;

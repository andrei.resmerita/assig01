package rest.builder;

import rest.dto.AccountDTO;
import rest.model.Account;

public class AccountBuilder {
    public Account buildAccountFromDTO(AccountDTO accountDTO) {
        Account account = new Account();
        account.setId(accountDTO.getId());
        account.setName(accountDTO.getName());
        account.setAddress(accountDTO.getAddress());
        return account;
    }
}

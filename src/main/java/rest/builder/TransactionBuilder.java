package rest.builder;

import rest.dto.TransactionDTO;
import rest.model.Transaction;

public class TransactionBuilder {

    public Transaction buildTransactionFromDTO(TransactionDTO transactionDTO) {
        Transaction transaction = new Transaction();
        transaction.setId(transactionDTO.getId());
        transaction.setAccount(transactionDTO.getAccount());
        transaction.setValue(transactionDTO.getValue());
        return transaction;
    }
}

package rest.controller;

import rest.builder.AccountBuilder;
import rest.dto.AccountDTO;
import rest.model.Account;
import rest.repository.impl.AccountDAOImpl;
import rest.service.AccountService;
import rest.service.impl.AccountServiceImpl;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/account")
public class AccountController {
    private final AccountService accountService;
    private final AccountBuilder accountBuilder;

    public AccountController() {
        this.accountService = new AccountServiceImpl();
        this.accountBuilder = new AccountBuilder();
    }

    @GET
    @Path("/{accountId}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getAccount(@PathParam("accountId") Integer accountId) {
        Account account = accountService.getAccount(accountId);
        return Response.ok(account).build();

    }

    @GET
    @Path("/list")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getAccounts() {
        return Response.ok(AccountDAOImpl.accountList).build();
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response createAccount(AccountDTO accountDTO) {
        Account account = accountBuilder.buildAccountFromDTO(accountDTO);
        accountService.createAccount(account);
        return Response.ok(account).build();
    }

    @PUT
    @Produces({MediaType.APPLICATION_JSON})
    public Response updateAccount(AccountDTO accountDTO) {
        Account account = accountService.updateAccount(accountDTO);
        return Response.ok(account).build();
    }

    @DELETE
    @Path("/{accountId}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response deleteAccount(@PathParam("accountId") Integer accountId) {
        accountService.deleteAccount(accountId);
        return Response.ok().build();
    }

    @GET
    @Path("/search")
    @Produces({MediaType.APPLICATION_JSON})
    public List<Account> searchAccount(@QueryParam("name") String name, @QueryParam("address") String address) {
        return accountService.search(name, address);
    }

}

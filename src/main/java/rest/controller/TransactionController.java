package rest.controller;

import rest.builder.TransactionBuilder;
import rest.dto.TransactionDTO;
import rest.model.Transaction;
import rest.repository.impl.TransactionDAOImpl;
import rest.service.TransactionService;
import rest.service.impl.TransactionServiceImpl;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/transaction")
public class TransactionController {
    private final TransactionService transactionService;
    private final TransactionBuilder transactionBuilder;

    public TransactionController() {
        transactionService = new TransactionServiceImpl();
        transactionBuilder = new TransactionBuilder();
    }

    @GET
    @Path("/{transactionId}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getTransaction(@PathParam("transactionId") Integer transactionId) {
        Transaction transaction = transactionService.getTransaction(transactionId);
        return Response.ok(transaction).build();

    }

    @GET
    @Path("/list")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getTransactions() {
        return Response.ok(TransactionDAOImpl.transactionList).build();
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    public Response createTransaction(TransactionDTO transactionDTO) {
        Transaction transaction = transactionBuilder.buildTransactionFromDTO(transactionDTO);
        transactionService.createTransaction(transaction);
        return Response.ok(transaction).build();
    }

    @PUT
    @Produces({MediaType.APPLICATION_JSON})
    public Response updateTransaction(TransactionDTO transactionDTO) {
        Transaction transaction = transactionService.updateTransaction(transactionDTO);
        return Response.ok(transaction).build();
    }

    @DELETE
    @Path("/{transactionId}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response deleteTransaction(@PathParam("transactionId") Integer transactionId) {
        transactionService.deleteTransaction(transactionId);
        return Response.ok().build();
    }

    @GET
    @Path("/search")
    @Produces({MediaType.APPLICATION_JSON})
    public List<Transaction> searchTransaction(@QueryParam("accountId") Integer id, @QueryParam("value") Integer value) {
        return transactionService.search(id, value);
    }
}
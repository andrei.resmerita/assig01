package rest.dto;

import rest.model.Account;

public class AccountDTO {


    private int id;
    private String name;
    private String address;

    public AccountDTO(int id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }

    public AccountDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Account toAccount() {
        Account result = new Account();
        result.setId(this.id);
        result.setName(this.name);
        result.setAddress(this.address);
        return result;
    }
}

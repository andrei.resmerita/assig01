package rest.dto;

import rest.model.Account;
import rest.model.Transaction;

public class TransactionDTO {

    private int id;
    private Account account;
    private int value;

    public TransactionDTO(int id, Account account, int value) {
        this.id = id;
        this.account = account;
        this.value = value;
    }

    public TransactionDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Transaction toTransaction() {
        Transaction transaction = new Transaction();
        transaction.setId(this.id);
        transaction.setAccount(this.account);
        transaction.setValue(this.value);
        return transaction;
    }
}

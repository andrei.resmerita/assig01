package rest.model;

public class Transaction {

    private int id;
    private Account account;
    private int value;

    public Transaction(int id, Account account, int value) {
        this.id = id;
        this.account = account;
        this.value = value;
    }

    public Transaction() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}

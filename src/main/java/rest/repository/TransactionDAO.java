package rest.repository;

import rest.dto.TransactionDTO;
import rest.model.Transaction;

import java.util.List;

public interface TransactionDAO {

    Transaction createTransaction(Transaction transaction);

    Transaction getTransaction(int id);

    Transaction updateTransaction(TransactionDTO transactionDTO);

    void deleteTransaction(int id);

    List<Transaction> search(int id, int value);

}

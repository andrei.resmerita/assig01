package rest.repository.impl;

import rest.dto.AccountDTO;
import rest.model.Account;
import rest.repository.AccountDAO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AccountDAOImpl implements AccountDAO {

    public static List<Account> accountList;

    public AccountDAOImpl() {
        accountList = new ArrayList<>();
        Account account = new Account();
        Account account2 = new Account();
        Account account3 = new Account();
        Account account4 = new Account();
        accountList.add(account);
        accountList.add(account2);
        accountList.add(account3);
        accountList.add(account4);
        int i = 0;
        int x = 1;
        List<String> address = new ArrayList<>(Arrays.asList("iasi", "bacau", "chisinau", "bucuresti"));
        List<String> name = new ArrayList<>(Arrays.asList("andrei", "daniel", "lulu", "robert"));
        for (Account a : accountList) {
            a.setId(x);
            a.setName(name.get(i));
            a.setAddress(address.get(i));
            i++;
            x++;
        }
    }

    @Override
    public Account createAccount(Account account) {
        accountList.add(account);
        return account;
    }

    @Override
    public Account getAccount(int id) {
        Account account = new Account();
        for (Account a : accountList) {
            if (a.getId() == id) {
                account = a;
            }
        }
        return account;
    }

    @Override
    public Account updateAccount(AccountDTO accountDTO) {
        Account account = getAccount(accountDTO.getId());
        account.setId(accountDTO.getId());
        account.setName(accountDTO.getName());
        account.setAddress(accountDTO.getAddress());
        return account;
    }

    @Override
    public void deleteAccount(int id) {
        accountList.remove(getAccount(id));
    }

    @Override
    public List<Account> search(String name, String address) {
        List<Account> result = new ArrayList<>();
        for (Account account : accountList) {
            if (account.getAddress().equals(address) || account.getName().equals(name)) {
                result.add(account);
            }
        }
        return result;
    }

}

package rest.repository.impl;

import rest.dto.TransactionDTO;
import rest.model.Account;
import rest.model.Transaction;
import rest.repository.TransactionDAO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TransactionDAOImpl implements TransactionDAO {
    public static List<Transaction> transactionList;

    public TransactionDAOImpl() {
        transactionList = new ArrayList<>();
        Transaction transaction = new Transaction();
        Transaction transaction2 = new Transaction();
        Transaction transaction3 = new Transaction();
        Transaction transaction4 = new Transaction();
        transactionList.add(transaction);
        transactionList.add(transaction2);
        transactionList.add(transaction3);
        transactionList.add(transaction4);
        int i = 0;
        int x = 1;
        List<Account> accountList = AccountDAOImpl.accountList;
        List<Integer> value = new ArrayList<>(Arrays.asList(100, 300, 150, 200));
        for (Transaction a : transactionList) {
            a.setId(x);
            a.setAccount(accountList.get(i));
            a.setValue(value.get(i));
            i++;
            x++;
        }
    }


    @Override
    public Transaction createTransaction(Transaction transaction) {
        transactionList.add(transaction);
        return transaction;
    }

    @Override
    public Transaction getTransaction(int id) {
        Transaction transaction = new Transaction();
        for (Transaction trans : transactionList) {
            if (trans.getId() == id) {
                transaction = trans;
            }
        }
        return transaction;
    }

    @Override
    public Transaction updateTransaction(TransactionDTO transactionDTO) {
        Transaction transaction = getTransaction(transactionDTO.getId());
        transaction.setId(transactionDTO.getId());
        transaction.setAccount(transactionDTO.getAccount());
        transaction.setValue(transactionDTO.getValue());
        return transaction;
    }

    @Override
    public void deleteTransaction(int id) {
        transactionList.remove(getTransaction(id));
    }

    @Override
    public List<Transaction> search(int id, int value) {
        List<Transaction> result = new ArrayList<>();
        for (Transaction transaction : transactionList) {
            if (transaction.getAccount().getId() == id || transaction.getValue() == value) {
                result.add(transaction);
            }
        }
        return result;
    }
}

package rest.service;

import rest.dto.AccountDTO;
import rest.model.Account;

import java.util.List;

public interface AccountService {

    Account createAccount(Account account);

    Account getAccount(int id);

    Account updateAccount(AccountDTO accountDTO);

    void deleteAccount(int id);

    List<Account> search(String name, String address);

}

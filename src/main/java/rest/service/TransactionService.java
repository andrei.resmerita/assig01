package rest.service;

import rest.dto.TransactionDTO;
import rest.model.Transaction;

import java.util.List;

public interface TransactionService {

    Transaction createTransaction(Transaction transaction);

    Transaction getTransaction(int id);

    Transaction updateTransaction(TransactionDTO transactionDTO);

    void deleteTransaction(int id);

    List<Transaction> search(int id, int value);

}

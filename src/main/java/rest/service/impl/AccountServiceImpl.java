package rest.service.impl;

import rest.dto.AccountDTO;
import rest.model.Account;
import rest.repository.AccountDAO;
import rest.repository.impl.AccountDAOImpl;
import rest.service.AccountService;

import java.util.List;

public class AccountServiceImpl implements AccountService {
    AccountDAO accountDAO = new AccountDAOImpl();

    @Override
    public Account createAccount(Account account) {
        return accountDAO.createAccount(account);
    }

    @Override
    public Account getAccount(int id) {
        return accountDAO.getAccount(id);
    }

    @Override
    public Account updateAccount(AccountDTO accountDTO) {
        return accountDAO.updateAccount(accountDTO);
    }

    @Override
    public void deleteAccount(int id) {
        accountDAO.deleteAccount(id);
    }

    @Override
    public List<Account> search(String name, String address) {
        return accountDAO.search(name, address);
    }
}

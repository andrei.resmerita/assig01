package rest.service.impl;

import rest.dto.TransactionDTO;
import rest.model.Transaction;
import rest.repository.TransactionDAO;
import rest.repository.impl.TransactionDAOImpl;
import rest.service.TransactionService;

import java.util.List;

public class TransactionServiceImpl implements TransactionService {
    TransactionDAO transactionDAO = new TransactionDAOImpl();

    @Override
    public Transaction createTransaction(Transaction transaction) {
        return transactionDAO.createTransaction(transaction);
    }

    @Override
    public Transaction getTransaction(int id) {
        return transactionDAO.getTransaction(id);
    }

    @Override
    public Transaction updateTransaction(TransactionDTO transactionDTO) {
        return transactionDAO.updateTransaction(transactionDTO);
    }

    @Override
    public void deleteTransaction(int id) {
        transactionDAO.deleteTransaction(id);
    }

    @Override
    public List<Transaction> search(int id, int value) {
        return transactionDAO.search(id, value);
    }
}

package streamsAndLambdas.capitalLetters;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class WithCapitalLetter {


    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("TREI");
        list.add("doi");
        System.out.println(new WithCapitalLetter().wordsWithCapitalLetter(list));
    }

    public String wordsWithCapitalLetter(List<String> list) {


        return list.stream().map(s -> s.toLowerCase(Locale.ROOT)).collect(Collectors.toList())
                .stream().map(s -> s.substring(0, 1).toUpperCase(Locale.ROOT) + s.substring(1)).collect(Collectors.joining(","));

    }
}

package streamsAndLambdas.countVowels;

import java.util.List;

@FunctionalInterface
public interface CountVowels {

    int countVowelsInString(List<String> list);

}

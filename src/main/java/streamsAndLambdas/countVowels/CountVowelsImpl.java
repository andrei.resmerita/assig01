package streamsAndLambdas.countVowels;

import java.util.List;
import java.util.Locale;

public class CountVowelsImpl {

    public int countVowelsFromString(List<String> list) {
        CountVowels countVowels = (n) -> (int) n.stream().map(s -> s.toLowerCase(Locale.ROOT)).flatMapToInt(s -> s.chars().filter(c -> c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u')).count();
        return countVowels.countVowelsInString(list);
    }
}


package streamsAndLambdas.iterating;

import streamsAndLambdas.iterating.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Iterate {

    public List<String> iterateUsingForEach(List<User> userList) {
        List<String> res = new ArrayList<>();
        userList.forEach(user -> res.add(user.getName()));
        return res;
    }

    public List<String> iterateUsingStreams(List<User> userList) {
        return userList.stream().map(user -> user.getName()).collect(Collectors.toList());
    }

}

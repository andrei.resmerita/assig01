package streamsAndLambdas.notification;

import streamsAndLambdas.notification.model.Email;

import java.util.List;
import java.util.concurrent.SubmissionPublisher;

public class NotificationService {
    private SubmissionPublisher<Email> publisher;

    private NotificationSubscriber subscriber;


    public NotificationService() {
        publisher = new SubmissionPublisher<>();
        subscriber = new NotificationSubscriber();
    }

    public SubmissionPublisher<Email> getPublisher() {
        return publisher;
    }


    public boolean notificationProcess(List<Email> emailList) {
        publisher.subscribe(subscriber);
        for (Email email : emailList) {
            publisher.submit(email);
        }

        while (emailList.size() != subscriber.getConsumedElements().size()) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
                return false;
            }
        }
        publisher.close();
        return true;
    }
}

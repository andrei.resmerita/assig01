package streamsAndLambdas.notification;

import streamsAndLambdas.notification.model.Email;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Flow;

public class NotificationSubscriber implements Flow.Subscriber<Email> {
    private List<Email> consumedElements = new LinkedList<>();
    private Flow.Subscription subscription;


    public List<Email> getConsumedElements() {
        return consumedElements;
    }

    @Override
    public void onSubscribe(Flow.Subscription subscription) {
        this.subscription = subscription;
        this.subscription.request(1);
    }

    @Override
    public void onNext(Email email) {
        this.consumedElements.add(email);
        System.out.println("Sending the email to: " + email);
        this.subscription.request(1);

    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println("Error");
        throwable.printStackTrace();
    }

    @Override
    public void onComplete() {
        System.out.println("All emails sent");
    }


}

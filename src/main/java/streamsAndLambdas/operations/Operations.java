package streamsAndLambdas.operations;

import java.util.List;
import java.util.NoSuchElementException;

public class Operations {

    public int maxValue(List<Integer> list) {

        return list.stream().mapToInt(i -> i).max().orElseThrow(NoSuchElementException::new);
    }

    public int minValue(List<Integer> list) {
        if (list == null) {
            throw new NullPointerException("List empty");
        }
        return list.stream().mapToInt(i -> i).min().orElseThrow(NoSuchElementException::new);
    }

    public double averageValue(List<Integer> list) {
        return list.stream().mapToInt(i -> i).average().orElseThrow(NoSuchElementException::new);
    }
}

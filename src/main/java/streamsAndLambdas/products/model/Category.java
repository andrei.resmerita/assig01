package streamsAndLambdas.products.model;

public enum Category {
    SMARTHPHONE, TV, LAPTOP
}

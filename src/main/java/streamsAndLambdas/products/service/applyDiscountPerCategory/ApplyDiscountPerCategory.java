package streamsAndLambdas.products.service.applyDiscountPerCategory;

import streamsAndLambdas.products.model.Category;
import streamsAndLambdas.products.model.Product;

import java.util.List;

public interface ApplyDiscountPerCategory {


    List<Product> productsWithDiscount(List<Product> products, Category category, double discount);

}

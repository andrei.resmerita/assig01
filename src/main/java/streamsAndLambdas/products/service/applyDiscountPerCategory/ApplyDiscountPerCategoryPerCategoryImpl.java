package streamsAndLambdas.products.service.applyDiscountPerCategory;

import streamsAndLambdas.products.model.Category;
import streamsAndLambdas.products.model.Product;
import streamsAndLambdas.products.service.calculateDiscount.CalculateDiscount;
import streamsAndLambdas.products.service.calculateDiscount.CalculateDiscountImpl;

import java.util.List;

public class ApplyDiscountPerCategoryPerCategoryImpl implements ApplyDiscountPerCategory {
    CalculateDiscount calculateDiscount = new CalculateDiscountImpl();

    @Override
    public List<Product> productsWithDiscount(List<Product> products, Category category, double discount) {

        products.stream().filter(product -> product.getCategory().equals(category))
                .forEach(product -> product.setPrice(calculateDiscount.addDiscount(discount, product.getPrice())));
        return products;
    }


}

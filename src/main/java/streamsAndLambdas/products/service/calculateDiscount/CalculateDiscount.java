package streamsAndLambdas.products.service.calculateDiscount;

public interface CalculateDiscount {


     double addDiscount(double discount, double productPrice);
}

package streamsAndLambdas.products.service.calculateDiscount;

public class CalculateDiscountImpl implements CalculateDiscount {
    @Override
    public double addDiscount(double discount, double productPrice) {
        double discountCalculated = discount / 100 * productPrice;
        return productPrice - discountCalculated;
    }
}

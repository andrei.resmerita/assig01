package streamsAndLambdas.stringFiltering;


import java.util.List;
import java.util.stream.Collectors;

public class StringFiltering {

    public static int countVowels(String s) {
        int count = 0;
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u') {
                count++;
            }
        }
        return count;
    }

    public List<String> wordsStartingWithN(List<String> list) {
        return list.stream().filter(s -> s.startsWith("n")).collect(Collectors.toList());
    }

    public List<String> wordsEndingWithA(List<String> list) {
        return list.stream().filter(s -> s.endsWith("a")).collect(Collectors.toList());
    }

    public List<String> wordsLongerThan5Letters(List<String> list) {
        return list.stream().filter(s -> s.length() > 5).collect(Collectors.toList());
    }

    public List<String> wordsWithAtLeast3Vowels(List<String> list) {
        return list.stream().filter(s -> countVowels(s) >= 3).collect(Collectors.toList());
    }
}

package streamsAndLambdas.transactions.model;

import java.util.List;

public class Account {

    private String number;
    private String name;
    private String country;
    private List<Transaction> transactions;

    public Account(String number, String name, String country, List<Transaction> transactions) {
        this.number = number;
        this.name = name;
        this.country = country;
        this.transactions = transactions;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    @Override
    public String toString() {
        return "Account{" +
                "number='" + number + '\'' +
                ", name='" + name + '\'' +
                ", country='" + country + '\'' +
                ", transactions=" + transactions +
                '}';
    }
}
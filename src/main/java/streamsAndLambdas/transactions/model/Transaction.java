package streamsAndLambdas.transactions.model;

import java.util.Date;
import java.util.Objects;

public class Transaction {

    Account account;
    private Integer id;
    private Integer amount;
    private Date date;

    public Transaction(Integer id, Integer amount, Date date, Account account) {
        this.id = id;
        this.amount = amount;
        this.date = date;
        this.account = account;
    }

    public Integer getAccount() {
        return id;
    }

    public void setAccount(Integer id) {
        this.id = id;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", amount=" + amount +
                ", date=" + date +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return Objects.equals(account, that.account) && Objects.equals(id, that.id) && Objects.equals(amount, that.amount) && Objects.equals(date, that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(account, id, amount, date);
    }
}

package streamsAndLambdas.transactions.process;

import streamsAndLambdas.transactions.model.Account;
import streamsAndLambdas.transactions.model.Transaction;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TransactionProcess {


    public double getAverageTransactionAmount(List<Account> accountList) {

        List<Transaction> transactionList = accountList.stream().flatMap(account -> account.getTransactions().stream()).collect(Collectors.toList());
        return transactionList.stream().map(transaction -> transaction.getAmount())
                .mapToDouble(value -> value.doubleValue()).reduce(0, (partialSum, tValue) -> partialSum + tValue / transactionList.size());

    }

    public List<Transaction> getTop3TransactionsFromAllAccounts(List<Account> accountList) {

        return accountList.stream().flatMap(account -> account.getTransactions().stream())
                .sorted(Comparator.comparingInt(Transaction::getAmount).reversed()).limit(3).collect(Collectors.toList());

    }

    public Long totalTransactionAmountOfAMonth(List<Account> accountList, Date dateTime) {
        List<Transaction> transactionList = accountList.stream().flatMap(account -> account.getTransactions().stream()).collect(Collectors.toList());
        return transactionList.stream().filter(transaction -> transaction.getDate().equals(dateTime))
                .map(transaction -> transaction.getAmount()).reduce(0, (partialSum, tValue) -> partialSum + tValue).longValue();

    }

    public Map<String, List<Transaction>> getAccountAndTransactions(List<Account> accountList) {

        return accountList.stream().collect(Collectors.toMap(Account::getNumber, Account::getTransactions));
        //an alternative for the original request to use groupping for a Map<String,List<String> would be:
        // accountList.stream()
        // .collect(Collectors.groupingBy(Account::getNumber,Collectors.mapping(account -> account.getTransactions().toString(),Collectors.toList())));

    }


}

package threads.CronJobExecutor;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.*;

public class ExecuteTasks {

    public static void main(String[] args) throws InterruptedException, ExecutionException, IOException {
        File file = new File("src/cron.json");
        ReadFromJson readFromJson = new ReadFromJson();
        Task t1 = new Task(readFromJson.readFromJson(file));
        System.out.println(new ExecuteTasks().doTask(t1));
    }

    public boolean doTask(Task task) throws ExecutionException, InterruptedException {


        ExecutorService service = Executors.newFixedThreadPool(2);

        List<Callable<Boolean>> callables = new ArrayList<>(Collections.singletonList(task));

        List<Future<Boolean>> futures = service.invokeAll(callables);
        service.shutdown();

        return futures.get(0).get();
    }
}
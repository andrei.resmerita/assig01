package threads.CronJobExecutor;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ReadFromJson {
    public Map<String, String> readFromJson(File file) throws IOException {

        return new ObjectMapper().readValue(file, HashMap.class);

    }
}

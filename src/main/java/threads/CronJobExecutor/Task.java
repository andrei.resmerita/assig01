package threads.CronJobExecutor;


import java.util.Calendar;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Task implements Callable<Boolean> {
    private ExecutorService executorService;
    private Map<String, String> stringMap;
    private Calendar calendar;
    private String hour;
    private String minute;
    private String hm;

    public Task(Map<String, String> stringMap) {

        executorService = Executors.newCachedThreadPool();
        this.stringMap = stringMap;
        calendar = Calendar.getInstance();
        this.hour = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
        this.minute = String.valueOf(calendar.get(Calendar.MINUTE));
        this.hm = hour + ":" + minute;
    }

    public String getHm() {
        return hm;
    }

    public void setHm(String hm) {
        this.hm = hm;
    }

    public String doAnAction(Object mesg) {
        return "I am doing: +" + " " + mesg;
    }


    @Override
    public Boolean call() throws Exception {
        Thread.sleep(4000);
        for (Map.Entry<String, String> m : stringMap.entrySet()) {

            if (hm.equals(m.getKey())) {
                Object sample = m.getValue();
                System.out.println(doAnAction(sample));
                return true;

            } else if (stringMap.size() == 0) {
                throw new RuntimeException("Empty tasks list");
            }
        }
        return false;
    }
}

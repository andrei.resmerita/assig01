package threads.deadlocks;

import threads.syncronized.Performance;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

public class Deadlock {

    public boolean startingDeadlock(IAmDoingJobThread iAmDoingJobThread, IWillBeInDeadLockThread iWillBeInDeadLockThread, Performance n) throws InterruptedException {
        iAmDoingJobThread = new IAmDoingJobThread(n, 10);
        iWillBeInDeadLockThread = new IWillBeInDeadLockThread(n, 10);
        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(2);
        Callable<Long> c1 = iAmDoingJobThread;
        Callable<Long> c2 = iWillBeInDeadLockThread;
        List<Callable<Long>> callables = new ArrayList<>(Arrays.asList(c1, c2));

        List<Future<Long>> futures = executor.invokeAll(callables);
        return futures.size() == 2;
    }
}

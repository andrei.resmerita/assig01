package threads.deadlocks;

import threads.syncronized.Performance;

import java.util.concurrent.Callable;

public class IAmDoingJobThread implements Callable<Long> {
    Performance n;
    int x;

    public IAmDoingJobThread(Performance n, int x) {
        this.n = n;
        this.x = x;
    }

    @Override
    public Long call() throws Exception {
        System.out.println("Working to starve the other thread");

        long time = System.currentTimeMillis();
        n.printNumber(x);
        Thread.sleep(5000);
        return System.currentTimeMillis() - time;
    }

}

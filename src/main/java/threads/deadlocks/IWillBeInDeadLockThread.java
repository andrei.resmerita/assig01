package threads.deadlocks;

import threads.syncronized.Performance;

import java.util.concurrent.Callable;

public class IWillBeInDeadLockThread implements Callable<Long> {

    static long time;
    Performance n;
    int x;

    public IWillBeInDeadLockThread(Performance n, int x) {
        this.n = n;
        this.x = x;
    }

    @Override
    public Long call() throws Exception {

        System.out.println("Starting deadlock");

        long time = System.currentTimeMillis();
        n.printNumber(x);
        return System.currentTimeMillis() - time;

    }
}

package threads.numberSearch;

import java.util.List;
import java.util.concurrent.Callable;

public class MyThread1 implements Callable<Boolean> {
     Boolean result;
    Integer start;
    Integer end;
    List<Integer> list;
    private Integer num;

    public MyThread1(Integer start, Integer end, List<Integer> list, Integer num) {
        this.start = start;
        this.end = end;
        this.list = list;
        this.num = num;
    }

    public Integer getNum() {
        return num;
    }

    @Override
    public synchronized Boolean call() throws Exception {
        for (int i = start; i <= end; i++) {
            Integer res = list.get(i);
            if (res.equals(num)) {
                return true;
            }
        }

        return false;
    }
}

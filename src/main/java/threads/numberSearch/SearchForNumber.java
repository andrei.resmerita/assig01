package threads.numberSearch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

public class SearchForNumber {
    public boolean searchForNumber(List<Integer> list, int number, int start, int end) throws ExecutionException, InterruptedException {

        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(4);

        Callable<Boolean> callable1 = new MyThread1(start, end / 4, list, number);

        Callable<Boolean> callable2 = new MyThread1(end / 4, end / 3, list, number);

        Callable<Boolean> callable3 = new MyThread1(end / 3, end / 2, list, number);

        Callable<Boolean> callable4 = new MyThread1(end / 2, end, list, number);

        List<Callable<Boolean>> callables = new ArrayList<>(Arrays.asList(callable1, callable2, callable3, callable4));
        for (Callable<Boolean> callable : callables) {
            Future<Boolean> future = executor.submit(callable);
            if (future.get().equals(true)) {
                return true;
            }
        }

        executor.shutdown();
        return false;

    }
}

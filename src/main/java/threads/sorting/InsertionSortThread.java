package threads.sorting;

import java.util.List;
import java.util.concurrent.Callable;

public class InsertionSortThread implements Callable<List<Integer>> {

    private List<Integer> list;

    public InsertionSortThread(List<Integer> list) {
        this.list = list;
    }


    @Override
    public synchronized List<Integer> call() throws Exception {
        for (int j = 1; j < list.size(); j++) {
            Integer current = list.get(j);
            int i = j - 1;
            while ((i > -1) && ((list.get(i).compareTo(current)) == 1)) {
                list.set(i + 1, list.get(i));
                i--;
            }
            list.set(i + 1, current);

        }
        System.out.println("threadI finisehd");

        return list;
    }
}

package threads.sorting;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;

public class QuickSortJavaThread implements Callable<List<Integer>> {
    private List<Integer> list;

    public QuickSortJavaThread(List<Integer> list) {
        this.list = list;
    }


    @Override
    public synchronized List<Integer> call() throws Exception {
        Collections.sort(list);
        System.out.println("threadQ finisehd");

        return list;

    }
}

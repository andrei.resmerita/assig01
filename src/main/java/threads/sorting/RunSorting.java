package threads.sorting;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;

public class RunSorting {

    public static void main(String[] args) throws InterruptedException {
        RunSorting main = new RunSorting();
        main.runSorting();
    }

    public void runSorting() throws InterruptedException {
        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(3);

        List<Future<List<Integer>>> resultList = new ArrayList<>();

        Random random = new Random();
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            list.add(random.nextInt(1000) + 1);
        }


        Callable<List<Integer>> callable1 = new InsertionSortThread(list);
        Callable<List<Integer>> callable2 = new ThreadBubbleSort(list);

        Callable<List<Integer>> callable3 = new QuickSortJavaThread(list);
        List<Callable<List<Integer>>> callables = new ArrayList<>();
        callables.add(callable1);
        callables.add(callable2);
        callables.add(callable3);

        System.out.println(System.nanoTime() + " before a InsertionSort");
        executor.submit(callable1);
        System.out.println(System.nanoTime() + " after insertionSort");

        TimeUnit.SECONDS.sleep(1);

        System.out.println(System.nanoTime() + " before BubbleSorrt");
        executor.submit(callable2);
        System.out.println(System.nanoTime() + " after a bubbbleSort");

        TimeUnit.SECONDS.sleep(1);

        System.out.println(System.nanoTime() + " before CollectionSort");
        executor.submit(callable3);
        System.out.println(System.nanoTime() + " after collectionSort");


        executor.shutdown();
    }
}

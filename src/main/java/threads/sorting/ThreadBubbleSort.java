package threads.sorting;

import java.util.List;
import java.util.concurrent.Callable;

public class ThreadBubbleSort implements Callable<List<Integer>> {

    private List<Integer> list;

    public ThreadBubbleSort(List<Integer> list) {
        this.list = list;
    }


    @Override
    public synchronized List<Integer> call() throws Exception {
        Integer temp;
        boolean sorted = false;

        while (!sorted) {
            sorted = true;
            for (int i = 0; i < list.size() - 1; i++) {
                if (list.get(i).compareTo(list.get(i + 1)) > 0) {
                    temp = list.get(i);
                    list.set(i, list.get(i + 1));
                    list.set(i + 1, temp);
                    sorted = false;
                }
            }
        }
        System.out.println("threadb finisehd");
        return list;
    }
}

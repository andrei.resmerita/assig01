package threads.starvation;

public class DoingSomeAction {
    static int c;

    public DoingSomeAction() {
        c = 0;
    }

    public synchronized void work() {
        while (true) {
            c++;
            if (!Thread.currentThread().isInterrupted()) {
                throw new RuntimeException("error");
            }
            System.out.println(c);
        }
    }
}

package threads.starvation;

public class StarvationExemple extends Thread {

    DoingSomeAction w;

    public StarvationExemple(DoingSomeAction w) {
        this.w = w;
    }

    public void run() {
        w.work();
    }
}

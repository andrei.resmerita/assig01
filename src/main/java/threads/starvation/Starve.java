package threads.starvation;

public class Starve {
    public boolean doingStarvation() throws InterruptedException {
        DoingSomeAction w = new DoingSomeAction();
        StarvationExemple starvationExemple = new StarvationExemple(w);
        StarvationExemple starvationExemple2 = new StarvationExemple(w);
        StarvationExemple starvationExemple3 = new StarvationExemple(w);
        StarvationExemple[] exemples = new StarvationExemple[]{starvationExemple, starvationExemple2, starvationExemple3};
        for (int i = 0; i < exemples.length; i++) {
            exemples[i + 2].start();

        }
        return false;
    }


}

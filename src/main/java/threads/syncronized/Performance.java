package threads.syncronized;

public class Performance {

    public static void main(String[] args) throws InterruptedException {
        Performance n = new Performance();
        Thread1 t = new Thread1(n, 50);
        Thread2 t2 = new Thread2(n, 10);
        System.out.println(new Performance().getTime(t, t2));
    }

    public void printNumber(int n) {
        for (int i = 0; i < n; i++) {
            System.out.println(i);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean getTime(Thread1 t1, Thread2 t2) throws InterruptedException {
        Performance n = new Performance();
        //   Thread1 t = new Thread1(n, 50);
        // Thread2 t2 = new Thread2(n, 10);
        long r1 = System.currentTimeMillis();
        t1.start();
        long r2 = System.currentTimeMillis();

        t1.join();
        long r3 = System.currentTimeMillis();

        t2.start();
        long r4 = System.currentTimeMillis();
        return r4 > r2;

    }
}

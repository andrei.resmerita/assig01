package threads.syncronized;

public class Thread1 extends Thread {
    private Performance n;
    private int x;

    public Thread1(Performance n, int x) {
        this.n = n;
        this.x = x;
    }

    @Override
    public void run() {
        System.out.println("executing Thread1");
        n.printNumber(x);
    }
}

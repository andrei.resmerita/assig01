package threads.syncronized;

public class Thread2 extends Thread {
    Performance n;
    int x;

    public Thread2(Performance n, int x) {
        this.n = n;
        this.x = x;
    }

    @Override
    public void run() {
        System.out.println("Executing thread2");
        n.printNumber(x);
    }
}

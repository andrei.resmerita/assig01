package threads.wordOccurrences;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class CountWordOccurences {


    public Integer numberOfOccurences(List<String> s, String word) throws ExecutionException, InterruptedException {

        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(4);
        Callable<Integer> c1 = new WordOccurrences(word, s, 0, 3);
        Callable<Integer> c2 = new WordOccurrences(word, s, 3, 6);
        Callable<Integer> c3 = new WordOccurrences(word, s, 6, 8);
        Callable<Integer> c4 = new WordOccurrences(word, s, 8, 10);
        Future<Integer> result1 = executor.submit(c1);

        Future<Integer> result2 = executor.submit(c2);

        Future<Integer> result3 = executor.submit(c3);

        Future<Integer> result4 = executor.submit(c4);


        List<Future<Integer>> resultList = new ArrayList<>();
        resultList.add(result1);
        resultList.add(result2);
        resultList.add(result3);
        resultList.add(result4);
        executor.shutdown();
        return WordOccurrences.count;


    }

}

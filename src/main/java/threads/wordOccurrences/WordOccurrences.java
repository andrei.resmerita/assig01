package threads.wordOccurrences;

import java.util.List;
import java.util.concurrent.Callable;

public class WordOccurrences implements Callable<Integer> {

    static int count;
    String word;
    List<String> stringList;
    int start;
    int end;

    public WordOccurrences(String word, List<String> stringList, int start, int end) {
        this.word = word;
        this.stringList = stringList;
        this.start = start;
        this.end = end;
        count = 0;

    }

    @Override
    public Integer call() throws Exception {
        for (int i = start; i < end; i++) {
            if (stringList.get(i).equals(word)) {
                count++;
            }
        }
        return count;
    }

}

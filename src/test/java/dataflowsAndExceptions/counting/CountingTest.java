package dataflowsAndExceptions.counting;

import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class CountingTest {

    CountImgAndPdf countImgAndPdf = new CountImgAndPdf();

    @Test
    public void countImgAndPdfTestSucces() {
        File f = new File("src/main/resources");
        int extractedRes = countImgAndPdf.numberOfFiles(f);

        Assertions.assertTrue(extractedRes == 3);
    }

    @Test
    public void countImgAndPdfTestInvalidInput() {
        File f = new File("no path");

        NullPointerException exc = Assert.assertThrows(NullPointerException.class,
                () -> countImgAndPdf.numberOfFiles(f));

        assertNotNull(exc);
    }

    @Test
    public void countImgAndPdfTestNoImgOrPdfSucces() {
        File f = new File("src/main/resources/text");
        int extractedRes = countImgAndPdf.numberOfFiles(f);

        Assertions.assertTrue(extractedRes == 0);
    }
}

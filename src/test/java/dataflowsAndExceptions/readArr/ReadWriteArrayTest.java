package dataflowsAndExceptions.readArr;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ReadWriteArrayTest {

    ReadWriteArray readWriteArray = new ReadWriteArray();
    String path;

    @Test
    public void readArrTestSucces() {

        path = "src/test/java/dataflowsAndExceptions/readArr/testReadArr.txt";
        String[] extractedRes = readWriteArray.readArr(path);
        String[] expected = new String[]{"word", "text", "some"};
        Assert.assertTrue(Arrays.equals(extractedRes, expected));

    }

    @Test
    public void readArrTestInvalidInput() {
        path = "nopath";
        NullPointerException exc = Assert.assertThrows(NullPointerException.class,
                () -> readWriteArray.readArr(path));
        assertNotNull(exc);
    }

    @Test
    public void writeReverseArr() {
        path = "src/test/java/dataflowsAndExceptions/readArr/testReadArr.txt";
        String[] extractedRes = readWriteArray.writeReverseArr(path);
        System.out.println(Arrays.toString(extractedRes));
        String[] expected = new String[]{"some", "text", "word"};
        Assert.assertTrue(Arrays.equals(extractedRes, expected));

    }

    @Test
    public void writeReverseArrTestInvalidInput() {
        path = "nopath";
        NullPointerException exc = Assert.assertThrows(NullPointerException.class,
                () -> readWriteArray.writeReverseArr(path));
        assertNotNull(exc);
    }
}

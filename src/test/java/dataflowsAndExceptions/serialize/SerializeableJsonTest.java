package dataflowsAndExceptions.serialize;

import com.fasterxml.jackson.databind.ObjectMapper;
import dataflowsAndExceptions.serialize.model.User;
import dataflowsAndExceptions.serialize.modelTestData.ModelTestData;
import dataflowsAndExceptions.serialize.serializeObj.impl.SerializeableJsonImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class SerializeableJsonTest {

    private SerializeableJsonImpl serializeableJson = new SerializeableJsonImpl();
    private ObjectMapper objectMapper = new ObjectMapper();
    private String path = "src/test/java/dataflowsAndExceptions/serialize";

    @Test
    public void serializeSucces() throws IOException {
        File file;
        file = new File(path + "/userTest.json");
        User user = ModelTestData.createUser();
        serializeableJson.serialize(user, file);

        User extractedUser = objectMapper.readValue(file, User.class);
        assertTrue(user.equals(extractedUser));
        file.delete();
    }

    @Test
    public void serializeNullObjectTest() throws IOException {
        User user = null;
        File file;
        file = new File(path + "/userTestNull.json");
        serializeableJson.serialize(user, file);
        User extractedUser = objectMapper.readValue(file, User.class);
        Assertions.assertNull(extractedUser);
        file.delete();
    }

    @Test
    public void deserializeTestSucces() throws IOException {
        File file = new File(path + "/userTest.json");
        User user = ModelTestData.createUser();
        objectMapper.writeValue(file, user);

        User extractedUser = serializeableJson.deserialize(file);
        assertTrue(user.equals(extractedUser));
        file.delete();
    }

}

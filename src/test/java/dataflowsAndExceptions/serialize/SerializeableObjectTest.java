package dataflowsAndExceptions.serialize;

import dataflowsAndExceptions.serialize.model.User;
import dataflowsAndExceptions.serialize.modelTestData.ModelTestData;
import dataflowsAndExceptions.serialize.serializeObj.impl.SerializeableObjectImpl;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class SerializeableObjectTest {

    private final String path = "src/test/java/dataflowsAndExceptions/serialize";
    private SerializeableObjectImpl objectSerializer = new SerializeableObjectImpl();

    @Test
    public void serializeSuccesTest() throws IOException, ClassNotFoundException {
        File file = new File(path + "/userTest.obj");
        User user = ModelTestData.createUser();

        objectSerializer.serialize(user, file);

        User extractedUser = (extractUser(file));

        assertTrue(user.equals(extractedUser));

    }

    @Test
    public void deserializeSuccesTest() {

        User user = ModelTestData.createUser();


        User extractedUser = objectSerializer.deserialize(new File("src/test/java/dataflowsAndExceptions/serialize/userTest.obj"));


        assertTrue(user.equals(extractedUser));

    }

    private User extractUser(File file) throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream(file);
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        User readUser = (User) objectInputStream.readObject();

        objectInputStream.close();
        fileInputStream.close();

        return readUser;
    }

}

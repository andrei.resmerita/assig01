package dataflowsAndExceptions.serialize;

import dataflowsAndExceptions.serialize.model.User;
import dataflowsAndExceptions.serialize.modelTestData.ModelTestData;
import dataflowsAndExceptions.serialize.serializeObj.impl.SerializeableXmlImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SerializeableXmlTest {

    private final String path = "src/test/java/dataflowsAndExceptions/serialize";
    private SerializeableXmlImpl serializeableXml = new SerializeableXmlImpl();
    private JAXBContext context;
    private Unmarshaller unmarshaller;
    private Marshaller marshaller;

    @BeforeAll
    public void setup() throws JAXBException {
        context = JAXBContext.newInstance(User.class);
        unmarshaller = context.createUnmarshaller();
        marshaller = context.createMarshaller();
    }

    @Test
    public void serializeSucces() throws JAXBException {
        File file = new File(path + "/userTest.xml");
        User user = ModelTestData.createUser();
        serializeableXml.serialize(user, file);

        User extractedUser = (User) unmarshaller.unmarshal(file);

        assertEquals(user, extractedUser);
        file.delete();

    }


    @Test
    public void deserializeTestSucces() throws JAXBException {
        File file = new File(path + "/userTest.xml");
        User user = ModelTestData.createUser();
        marshaller.marshal(user, file);

        User extractedUser = serializeableXml.deserialize(file);
        assertTrue(user.equals(extractedUser));
        file.delete();
    }

}

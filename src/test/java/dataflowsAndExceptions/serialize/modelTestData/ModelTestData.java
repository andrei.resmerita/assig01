package dataflowsAndExceptions.serialize.modelTestData;

import dataflowsAndExceptions.serialize.model.User;

public class ModelTestData {

    public static User createUser() {
        User user = new User();
        user.setName("andi");
        user.setId(1);
        user.setAddress("add");
        user.setCity("Iasi");
        user.setCnp(123);
        return user;
    }

}

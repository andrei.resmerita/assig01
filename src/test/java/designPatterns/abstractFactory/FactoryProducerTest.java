package designPatterns.abstractFactory;

import designPatterns.abstractfactory.contracts.AbstractFactory;
import designPatterns.abstractfactory.contracts.EntityManager;
import designPatterns.abstractfactory.main.FactoryProducer;
import designPatterns.abstractfactory.model.Connection;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class FactoryProducerTest {
    @Mock
    FactoryProducer factoryProducer = new FactoryProducer();


    @Test
    public void testGetCorrectFactory() {

        AbstractFactory abstractFactory = factoryProducer.getFactory("ORACLE");

        verify(factoryProducer, times(1)).getFactory("ORACLE");

    }

    @Test
    public void testGetCorrectDatabase() {
        FactoryProducer f1 = new FactoryProducer();
        AbstractFactory oracle = f1.getFactory("ORACLE");
        EntityManager entityManager = oracle.getEntityManager("ORACLE");
        Connection connection = new Connection("url", "pass");
        Connection extractedRes = entityManager.createConnection("url", "pass");

        Assertions.assertEquals(connection, extractedRes);
    }

    @Test
    public void testInvalidInput() {
        FactoryProducer f1 = new FactoryProducer();
        AbstractFactory oracle = f1.getFactory("zzz");

        Assertions.assertNull(oracle);
    }
}

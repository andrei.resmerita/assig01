package designPatterns.cor;

import designPatterns.cor.contract.Chain;
import designPatterns.cor.model.Account;
import designPatterns.cor.model.ReciverAccount;
import designPatterns.cor.model.SenderAccount;
import designPatterns.cor.model.ValueValidation;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MainTests {

    Chain c1 = new SenderAccount();

    Chain c2 = new ReciverAccount();

    Chain c3 = new ValueValidation();
    String s = "sender";

    Account account = new Account(1, 3, 400, s);


    @Test
    public void testCorrectMethodInvocation() {
        c1.setNextChain(c2);
        c2.setNextChain(c3);
        System.out.println(account.getActionWanted());
        String extractedRes = c1.validate(account, s);
        System.out.println(extractedRes);
        String expectedRes = "sender";
        Assertions.assertEquals(expectedRes, extractedRes);
    }

    @Test
    public void testCorrectMethodInvocationNextChain() {
        c1.setNextChain(c2);
        c2.setNextChain(c3);
        String s1 = "reciver";
        Account account = new Account(3, 3, 400, s1);
        String extractedRes = c1.validate(account, s1);
        System.out.println(extractedRes);
        String expectedRes = "reciver";
        Assertions.assertEquals(expectedRes, extractedRes);
    }

    @Test
    public void testNoCorrectValidation() {
        String s2 = "error";
        Account account = new Account(34, 33, 400, s2);
        RuntimeException runtimeException = Assert.assertThrows(RuntimeException.class, () -> c1.validate(account, s2));
        Assertions.assertNotNull(runtimeException);
    }
}

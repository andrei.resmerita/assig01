package designPatterns.facade;

import designPatterns.facade.model.PayWithCard;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class PaymentProcessorTest {

    @Mock
    PaymentProcessor paymentProcessor = new PaymentProcessor();
    @Mock
    PayWithCard payWithCard = new PayWithCard();

    @Test
    public void testCorrectMethod() {
        paymentProcessor.setPayWithCard();
        payWithCard.pay();
        Mockito.verify(payWithCard, Mockito.times(1)).pay();

    }

    @Test
    public void testCorrectPaymentCard() {
        PaymentProcessor p1 = new PaymentProcessor();
        String extractedRes = p1.setPayWithCard();
        Assertions.assertEquals(extractedRes, "Card");
    }

    @Test
    public void testCorrectPaymentPhone() {
        PaymentProcessor p1 = new PaymentProcessor();
        String extractedRes = p1.setPayWithPhone();
        Assertions.assertEquals(extractedRes, "phone");
    }


}

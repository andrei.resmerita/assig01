package designPatterns.strategy.main;

import designPatterns.strategy.model.ReverseUsingSplit;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class StrategyCreationTest {
    @Mock
    StrategyCreation strategyCreation = new StrategyCreation(new ReverseUsingSplit());
    String word = "word";
    String text = "word text";


    @Test
    public void testCorrectStrategy() {
        strategyCreation.executeStrategy(text, word);
        Mockito.verify(strategyCreation, Mockito.times(1)).executeStrategy(text, word);

    }

    @Test
    public void testCorrectResult() {
        StrategyCreation s1 = new StrategyCreation(new ReverseUsingSplit());
        String extractedRes = s1.executeStrategy(text, word);
        String expectedRes = "drow text";
        Assertions.assertEquals(expectedRes, extractedRes);
    }

    @Test
    public void testNullInputResult() {
        StrategyCreation s1 = new StrategyCreation(new ReverseUsingSplit());
        String extractedRes = s1.executeStrategy(null, null);
        String expectedRes = null;
        Assertions.assertEquals(expectedRes, extractedRes);
    }
}

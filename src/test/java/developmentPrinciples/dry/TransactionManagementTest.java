package developmentPrinciples.dry;

import developmentPrinciples.dry.correct.TransactionManagement;
import developmentPrinciples.dry.correct.model.Account;
import developmentPrinciples.dry.correct.model.Transaction;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TransactionManagementTest {
    TransactionManagement transactionManagement = new TransactionManagement();


    @Test
    public void getAverageAmountOfTransactionTestSucces() {
        Transaction t1 = new Transaction(100);
        Transaction t2 = new Transaction(100);
        List<Transaction> transactionList = new ArrayList<>(Arrays.asList(t1, t2));
        Account account = new Account(transactionList);
        int res = (int) transactionManagement.getAverageAmountTransactionValues(account);
        Assertions.assertEquals(100, res);
    }

    @Test
    public void getTotalAmountOfTransactionsSucces() {
        Transaction t1 = new Transaction(100);
        Transaction t2 = new Transaction(100);
        List<Transaction> transactionList = new ArrayList<>(Arrays.asList(t1, t2));
        Account account = new Account(transactionList);
        int res = transactionManagement.getTotalTransactionValue(account);
        Assertions.assertEquals(200, res);
    }
}

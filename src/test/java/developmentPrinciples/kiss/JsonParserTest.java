package developmentPrinciples.kiss;

import developmentPrinciples.kiss.correct.JsonParser;
import developmentPrinciples.kiss.correct.Person;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

public class JsonParserTest {

    JsonParser jsonParser = new JsonParser();

    @Test
    public void parseSucces() throws IOException {
        Person res = jsonParser.parse(new File("src/test/java/developmentPrinciples/kiss/p.json"));
        Person expectedP = new Person("Andi", "Iasi");
        Assertions.assertEquals(res, expectedP);
    }

}

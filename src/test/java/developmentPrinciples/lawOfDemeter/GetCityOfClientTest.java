package developmentPrinciples.lawOfDemeter;

import developmentPrinciples.lawOfDemeter.correct.Address;
import developmentPrinciples.lawOfDemeter.correct.Client;
import developmentPrinciples.lawOfDemeter.correct.GetCityOfClient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class GetCityOfClientTest {
    GetCityOfClient getCityOfClient = new GetCityOfClient();

    @Test
    public void getCityOfClientTestSucces() {
        Address address = new Address("Iasi");
        Client client = new Client("Andi", address);
        String res = getCityOfClient.getCityOfClient(client);
        Assertions.assertEquals(res, "Iasi");

    }
}

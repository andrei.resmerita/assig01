package developmentPrinciples.solid.dependencyInversion;

import developmentPrinciples.solid.dependencyInversion.correct.model.Customer;
import developmentPrinciples.solid.dependencyInversion.correct.service.CustomerService;
import developmentPrinciples.solid.dependencyInversion.correct.service.impl.CustomerServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CustomerServiceImplTest {

    CustomerService customerService = new CustomerServiceImpl();
    Customer customer = new Customer("Ion");

    @Test
    public void addCustomerTestSucces() {
        String name = "Ion";
        Customer c1 = customerService.addCustomer(name);
        Assertions.assertEquals(c1, customer);
    }

}

package developmentPrinciples.solid.interfaceSegregation;

import developmentPrinciples.solid.interfaceSegregation.correct.contract.SendPdfInvoice;
import developmentPrinciples.solid.interfaceSegregation.correct.impl.SendPdfInvoiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SendPdfInvoiceImplTest {

    SendPdfInvoice sendPdfInvoice = new SendPdfInvoiceImpl();


    @Test
    public void testSendingPdfInvoiceSucces() {
        String extractedRes = sendPdfInvoice.sendPdfInvoice();
        String expectedRes = "pdf";
        Assertions.assertEquals(expectedRes, extractedRes);
    }

}

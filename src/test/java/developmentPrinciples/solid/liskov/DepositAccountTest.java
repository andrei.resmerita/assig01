package developmentPrinciples.solid.liskov;

import developmentPrinciples.solid.liskov.correct.DepositAccount;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DepositAccountTest {

    DepositAccount depositAccount;


    @Test
    public void addMoneySucces() {
        depositAccount = new DepositAccount(100);
        int res = depositAccount.deposit(100);
        Assertions.assertEquals(res, 200);
    }
}

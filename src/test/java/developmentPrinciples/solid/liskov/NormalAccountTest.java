package developmentPrinciples.solid.liskov;

import developmentPrinciples.solid.liskov.correct.NormalAccount;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class NormalAccountTest {

    NormalAccount account;

    @Test
    public void testDepositSucces() {
        account = new NormalAccount(100);
        int extractedRes = account.deposit(100);
        Assertions.assertEquals(extractedRes, 200);
    }

    @Test
    public void testWithdrawSucces() {
        account = new NormalAccount(100);
        int extractedRes = account.withdraw(50);
        Assertions.assertEquals(extractedRes, 50);
    }

}

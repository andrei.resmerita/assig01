package developmentPrinciples.solid.openClose;

import developmentPrinciples.solid.openClose.correct.InterestRatecalculator;
import developmentPrinciples.solid.openClose.correct.PersonalInterestRateCalculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PersonalInterestRateCalculatorTest {

    @Test
    public void testCalculateSucces() {
        InterestRatecalculator interestRatecalculator = new PersonalInterestRateCalculator(5);
        interestRatecalculator.setValue(100);
        interestRatecalculator.setInterestRate(5);
        int res = interestRatecalculator.calculate(interestRatecalculator.getValue());
        Assertions.assertEquals(res, 495);
    }
}

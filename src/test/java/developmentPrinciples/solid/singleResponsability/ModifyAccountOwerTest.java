package developmentPrinciples.solid.singleResponsability;

import developmentPrinciples.solid.singleResponsability.correct.Account;
import developmentPrinciples.solid.singleResponsability.correct.ModifyAccountOwner;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ModifyAccountOwerTest {
    ModifyAccountOwner modifyAccountOwner = new ModifyAccountOwner();

    @Test
    public void modifyAccountOwnerTestSucces() {
        Account account = new Account(1l, "3", "Andi");
        modifyAccountOwner.modifyAccountNumber(account, "Name");
        Assertions.assertEquals(account, new Account(1l, "3", "Name"));

    }
}

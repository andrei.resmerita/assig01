package developmentPrinciples.yagni;

import developmentPrinciples.yagni.correct.UpdateAccount;
import developmentPrinciples.yagni.correct.model.Account;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class UpdateAccountTest {
    UpdateAccount updateAccount = new UpdateAccount();

    @Test
    public void updateAccountTestSucces() {
        Account account = new Account("Name");
        Account extractedRes = updateAccount.updateAccount(account, "Andi");
        Assertions.assertEquals(extractedRes, new Account("Andi"));
    }
}

package fundamentals;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;


public class BubbleSortTest {

    private fundamentals.BubbleSortImpl bubbleSort = new BubbleSortImpl();


    @Test
    public void testBubbleSortSucces() {
        int[] arr = {5, 4, 3, 2, 1};
        int[] resu = this.bubbleSort.bubbleSort(arr);
        int[] expected = {1, 2, 3, 4, 5};
        assertArrayEquals(resu, expected);

    }


}

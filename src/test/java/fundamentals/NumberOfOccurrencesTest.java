package fundamentals;

import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

public class NumberOfOccurrencesTest {

    NumberOfOccurrences numberOfOccurrences = new NumberOfOccurrences();

    @Test
    public void numberOfOccurrencesTestSuccesInt(){

        int[] ar={1,2,1};
        int number = 1;

        int res = this.numberOfOccurrences.numberOfOccurrences(ar,number);

        Assertions.assertTrue(res==2);

    }
@Test
    public void NumberOfOccurrencesTestStringSucces(){
        String[] arr={"UNU","DOI","UNU"};
        String word = "UNU";

        int res = this.numberOfOccurrences.numberOfOccurrences(arr,word);

        Assert.assertTrue(res==2);

    }

    @Test
    public void NumberOfOccurrencesTestInvalidInput(){
        int[]arr=null;

        int res = this.numberOfOccurrences.numberOfOccurrences(arr,5);

        Assertions.assertTrue(res==0);
    }


}

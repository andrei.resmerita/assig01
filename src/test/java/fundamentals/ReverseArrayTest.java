package fundamentals;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class ReverseArrayTest {

    private ReverseArray reverseArray = new ReverseArray();


    @Test
    public void reverseArrTestSucces(){

        int[] arr = {1,2,3,4,5};

        int[] res = this.reverseArray.reverseArray(arr);

        Assert.assertTrue(Arrays.equals(res, new int[]{5, 4, 3, 2, 1}));
    }


    @Test
    public void reverseArrTestNull(){
        int[] arr = null;

        int[] res=this.reverseArray.reverseArray(arr);

        Assert.assertNull(res);
    }
}

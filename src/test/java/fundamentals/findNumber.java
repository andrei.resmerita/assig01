package fundamentals;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class findNumber {
    RandomArraySearch randomArraySearch = new RandomArraySearch();

    @Test
    public void testFindNumberSuccess() {
        int[] array = new int[]{1, 2, 1, 3, 4};
        int number = 1;

        int index = randomArraySearch.findNumber(array, number);

        assertEquals(index, 1);
    }

    @Test
    public void testFindNumberNotPresent() {
        int[] array = new int[]{1, 2, 1, 3, 4};
        int number = 5;

        int index = randomArraySearch.findNumber(array, number);

        assertEquals(index, -1);
    }

    @Test
    public void testFindElementInvalidInputException() {
        int[] array = null;
        int number = 10;

        NullPointerException exception = Assert.assertThrows(NullPointerException.class,
                () -> randomArraySearch.findNumber(array, number));

        assertNotNull(exception);
    }

}

package genericsAndCollections.dictionary;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ReadFromDicitonaryTest {
    ReadFromDictionary readFromDictionary = new ReadFromDictionary();
    File file;

    @BeforeAll
    public void setup() {
        file = new File("src/test/resources/dictionary.json");
    }

    @Test
    public void readFromDictionarySucces() throws IOException {
        Map<String, List<String>> expectedMap = new HashMap<>();
        List<String> list1 = new ArrayList<>();
        list1.add("Nebunie");
        list1.add("Faptă sau vorbă de om nebun");
        List<String> list2 = new ArrayList<>();
        list2.add("Capacitatea unui corp de a reveni la forma și dimensiunea inițială după deformare.");
        list2.add("Capacitatea cuiva de a reveni la normalitate după suferirea unui șoc (emoțional, economic șamd)");
        expectedMap.put("INSANITÁTE", list1);
        expectedMap.put("REZILIENTA", list2);

        Map<String, List<String>> extractedMap = readFromDictionary.readFromJson(file);

        Assertions.assertEquals(expectedMap, extractedMap);

    }

}

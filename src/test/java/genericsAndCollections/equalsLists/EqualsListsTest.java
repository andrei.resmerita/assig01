package genericsAndCollections.equalsLists;

import genericsAndCollections.lists.EqualsList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.ArrayList;
import java.util.List;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class EqualsListsTest {


    List<Integer> list1;
    List<Integer> list2;

    @BeforeEach
    public void setup() {
        list1 = new ArrayList<>();
        list2 = new ArrayList<>();

    }


    @Test
    public void equalsListsSameOrderSucces() {
        list1.add(2);
        list1.add(5);
        list2.add(2);
        list2.add(5);

        boolean extractedRes = EqualsList.areEquals(list1, list2);
        boolean expectedRes = true;

        Assertions.assertEquals(extractedRes, expectedRes);

    }

    @Test
    public void equalsListsNotSameOrderSucces() {
        list1.add(2);
        list1.add(5);
        list2.add(5);
        list2.add(2);

        boolean extractedRes = EqualsList.areEquals(list1, list2);
        boolean expectedRes = true;

        Assertions.assertEquals(extractedRes, expectedRes);

    }


    @Test
    public void equalsListsNotEqualsSucces() {
        list1.add(2);
        list1.add(5);
        list2.add(5);
        list2.add(7);

        boolean extractedRes = EqualsList.areEquals(list1, list2);
        boolean expectedRes = true;

        Assertions.assertNotEquals(extractedRes, expectedRes);

    }

    @Test
    public void equalsListsOneNullSucces() {
        list1.add(2);
        list1.add(5);

        boolean extractedRes = EqualsList.areEquals(list1, list2);
        boolean expectedRes = false;

        Assertions.assertEquals(extractedRes, expectedRes);

    }

}

package genericsAndCollections.queue;

import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class QueueTest {

    @Test
    public void createEmptyQueue() {
        Queue<String> queue = new Queue<>();
        int expected = 0;
        Assertions.assertEquals(expected, queue.getSize());
    }

    @Test
    public void addElementInQueue() {
        Queue<String> queue = new Queue<>();
        queue.add("Unu");
        int expectedSize = 1;
        Assertions.assertEquals(expectedSize, queue.getSize());
    }

    @Test
    public void addElementInQueueCorrectOrder() {
        Queue<String> queue = new Queue<>();
        queue.add("Unu");
        queue.add("Doi");
        queue.add("Trei");
        Object[] arr = queue.getArr();
        Object expectedRes = arr[0];
        String extractedRes = queue.get();
        Assertions.assertEquals(expectedRes, extractedRes);
    }

    @Test
    public void removeFirstElementSucces() {
        Queue<Integer> queue = new Queue<>();
        queue.add(1);
        queue.add(15);
        queue.add(9);
        Object[] arr = queue.getArr();
        Object expectedRes = arr[0];
        int extractedRes = queue.remove();
        Assertions.assertEquals(expectedRes, extractedRes);
    }


    @Test
    public void removeFirstElementEmptyQueue() {
        Queue<Integer> queue = new Queue<>();

        RuntimeException exception = Assert.assertThrows(RuntimeException.class,
                queue::remove);

        assertNotNull(exception);
    }

    @Test
    public void getFirstFromQueue() {
        Queue<Integer> queue = new Queue<>();
        queue.add(19);
        queue.add(3);
        queue.add(7);

        int extractedRes = queue.get();
        int expectedRes = 19;
        Assertions.assertEquals(expectedRes, extractedRes);
    }


    @Test
    public void getElementFromQueueEmptyQueue() {
        Queue<Integer> queue = new Queue<>();

        RuntimeException exception = Assert.assertThrows(RuntimeException.class, queue::get);
        Assertions.assertNotNull(exception);
    }

    @Test
    public void isEmptyQueue() {
        Queue<Integer> queue = new Queue<>();

        boolean expected = true;

        Assertions.assertEquals(expected, queue.isEmpty());


    }

    @Test
    public void listValuesFromQueue() {
        Queue<Integer> queue = new Queue<>();
        queue.add(5);
        queue.add(14);
        queue.add(3);

        Object[] arr = queue.list(queue);
        Integer[] extractedRes = new Integer[arr.length];
        System.arraycopy(arr, 0, extractedRes, 0, arr.length);
        Integer[] expectedRes = new Integer[]{5, 14, 3};

        Assertions.assertArrayEquals(extractedRes, expectedRes);
    }

    @Test
    public void listValuesFromQueueEmptyQueue() {
        Queue<Object> queue = new Queue<>();
        RuntimeException exception = Assert.assertThrows(RuntimeException.class, () -> queue.list(queue));
        Assertions.assertNotNull(exception);

    }


}

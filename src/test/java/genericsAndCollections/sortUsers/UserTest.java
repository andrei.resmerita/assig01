package genericsAndCollections.sortUsers;

import genericsAndCollections.sortUsers.model.User;
import genericsAndCollections.sortUsers.sortUserArray.SortUserArray;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.ArrayList;
import java.util.List;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class UserTest {

    SortUserArray sortUserArray = new SortUserArray();
    List<User> listTest = new ArrayList<>();

    User u1;
    User u2;
    User u3;

    @BeforeAll
    public void setup() {
        u1 = new User("Name1", 11, "b");
        u2 = new User("Name2", 2, "a");
        u3 = new User("Name3", 31, "c");
        listTest.add(u1);
        listTest.add(u2);
        listTest.add(u3);

    }

    @Test
    public void sortUserByAgeTestSucces() {
        List<User> listWithExpectedOrder = new ArrayList<>();
        listWithExpectedOrder.add(u2);
        listWithExpectedOrder.add(u1);
        listWithExpectedOrder.add(u3);

        List<User> extractedList = sortUserArray.sortUsersByAge(listTest);

        boolean res = listWithExpectedOrder.equals(extractedList);

        Assertions.assertTrue(res);
    }

    @Test
    public void sortUserByAgeTestNotCorrectlySortedByAge() {

        List<User> listWithExpectedOrder = new ArrayList<>();
        listWithExpectedOrder.add(u1);
        listWithExpectedOrder.add(u2);
        listWithExpectedOrder.add(u3);

        List<User> extractedList = sortUserArray.sortUsersByAge(listTest);

        boolean res = listWithExpectedOrder.equals(extractedList);

        Assertions.assertFalse(res);
    }

    @Test
    public void sortUserByAgeNullPointerExc() {
        NullPointerException exc = Assert.assertThrows(NullPointerException.class,
                () -> sortUserArray.sortUsersByAge(null));

        Assertions.assertNotNull(exc);
    }

}

package genericsAndCollections.stack;

import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class StackTest<T> {


    @Test
    public void createEmptyStack() {
        Stack<String> stack = new Stack<>();
        int expected = 0;
        Assertions.assertEquals(expected, stack.getSize());
    }

    @Test
    public void addElementInStack() {
        Stack<String> stack = new Stack<>();
        stack.add("Unu");
        int expectedSize = 1;
        Assertions.assertEquals(expectedSize, stack.getSize());
    }

    @Test
    public void addElementInStackCorrectOrder() {
        Stack<String> stack = new Stack<>();
        stack.add("Unu");
        stack.add("Doi");
        stack.add("Trei");

        Object[] arr = stack.getArr();
        Object expectedRes = arr[2];
        String extractedRes = stack.get();
        Assertions.assertEquals(expectedRes, extractedRes);
    }

    @Test
    public void removeLastElementSucces() {
        Stack<Integer> stack = new Stack<>();
        stack.add(1);
        stack.add(15);
        stack.add(9);

        Object[] arr = stack.getArr();
        Object expectedRes = arr[2];
        int extractedRes = stack.remove();
        Assertions.assertEquals(expectedRes, extractedRes);
    }


    @Test
    public void removeLastElementEmptyStack() {
        Stack<Integer> stack = new Stack<>();
        Object[] arr = stack.getArr();
        RuntimeException exception = Assert.assertThrows(RuntimeException.class,
                stack::remove);

        assertNotNull(exception);
    }

    @Test
    public void getElementFromStack() {
        Stack<Integer> stack = new Stack<>();
        stack.add(19);
        stack.add(3);
        stack.add(7);

        int extractedRes = stack.get();
        int expectedRes = 7;
        Assertions.assertEquals(expectedRes, extractedRes);
    }


    @Test
    public void getElementFromStackEmptyStack() {
        Stack<Integer> stack = new Stack<>();

        RuntimeException exception = Assert.assertThrows(RuntimeException.class, stack::get);
        Assertions.assertNotNull(exception);
    }

    @Test
    public void isEmptyStack() {
        Stack<Integer> stack = new Stack<>();

        boolean expected = true;

        Assertions.assertEquals(expected, stack.isEmpty());


    }

    @Test
    public void listValuesFromStack() {
        Stack<Integer> stack = new Stack<>();
        stack.add(5);
        stack.add(14);
        stack.add(3);
        System.out.println(Arrays.toString(stack.getArr()) + "aiaic");
        Object[] arr = stack.list(stack);
        Integer[] extractedRes = new Integer[arr.length];
        System.arraycopy(arr, 0, extractedRes, 0, arr.length);
        Integer[] expectedRes = new Integer[]{5, 14, 3};

        Assertions.assertArrayEquals(extractedRes, expectedRes);
    }

    @Test
    public void listValuesFromStackEmptyStack() {

        Stack<Object> stack = new Stack<>();

        RuntimeException exception = Assert.assertThrows(RuntimeException.class, () -> stack.list(stack));
        Assertions.assertNotNull(exception);

    }

}

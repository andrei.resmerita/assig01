package oop.text;

import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class reverseTest {

    private String word;
    private String text;
    private Text textClass;
    private String res;

    @BeforeEach
    public void setup() {
        this.res = "meroL ipsum dolor sit amet, consectetur adipiscing elit. Maecenas congue.";
        this.textClass = new Text();
        this.word = "Lorem";
        this.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas congue.";
    }

    @Test
    public void reverseWordFromTextTestSucces2Params() {

        String extractedWord = this.textClass.reverse(word, text);

        Assertions.assertEquals(extractedWord, res);

    }

    @Test
    public void reverseWordFromTextTestSucces3Params() {
        int start = 0;

        String extractedWord = this.textClass.reverse(word, text, start);

        Assert.assertTrue(extractedWord.equals(res));

    }

    @Test
    public void reverseWordFromTextTestSucces4Params() {
        int start = 0;
        int end = text.length() - 1;

        String extractedWord = this.textClass.reverse(word, text, start, end);
        Assert.assertTrue(extractedWord.equals(res));
    }


    @ParameterizedTest
    @ValueSource(strings = {"", " "})
    public void reverseWordFromTextNull2Params(String input) {
        String extractedWord = this.textClass.reverse(input, text);

        Assert.assertNull(extractedWord);
    }

    @ParameterizedTest
    @ValueSource(strings = {"", " "})
    public void reverseWordFromTextNull3Params(String input) {

        String extractedWord = this.textClass.reverse(input, text, 0);

        Assert.assertNull(extractedWord);
    }

    @ParameterizedTest
    @ValueSource(strings = {"", " "})
    public void reverseWordFromTextNull4Params(String input) {
        int end = text.length() - 1;
        String extractedWord = this.textClass.reverse(input, text, 0, end);

        Assert.assertNull(extractedWord);
    }

    @Test
    public void reverseWordFromTextWrongInput3Params() {
        int start = 3;

        String extractedWord = this.textClass.reverse(word, text, start);

        Assert.assertNull(extractedWord);
    }

    @Test
    public void reverseWordFromTextWrongInput4Params() {
        int start = 3;
        int end = 4;

        String extractedWord = this.textClass.reverse(word, text, start, end);

        Assert.assertNull(extractedWord);
    }
}

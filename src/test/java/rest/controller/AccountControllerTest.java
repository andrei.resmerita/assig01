package rest.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import rest.model.Account;

import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AccountControllerTest {

    ObjectMapper objectMapper = new ObjectMapper();
    Long expectedId = 1L;

    @Test
    public void testGetAccountReturnedIdValue() {
        Response res = RestAssured
                .given()
                .get("http://localhost:8080/Gradle___mambu_internship_assignements_war/account/1")
                .then().extract().response();
        String extracted = res.jsonPath().getString("id");
        assertTrue(extracted.equals("1"));
    }

    @Test
    public void testGetAccountById() {
        RestAssured
                .given()
                .get("http://localhost:8080/Gradle___mambu_internship_assignements_war/account/" + expectedId)
                .then()
                .statusCode(200)
                .body("id", CoreMatchers.equalTo(1))
                .body("name", CoreMatchers.equalTo("andrei"));
    }

    @Test
    public void testCreateNewAccount() throws JsonProcessingException {
        Account account = new Account(10, "ana", "iasi");
        RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(objectMapper.writeValueAsString(account))
                .post("http://localhost:8080/Gradle___mambu_internship_assignements_war/account")
                .then()
                .statusCode(200)
                .body("id", CoreMatchers.equalTo(10))
                .time(lessThan(1L), TimeUnit.SECONDS);
    }

    @Test
    public void testGetAccountList() throws JsonProcessingException {
        RestAssured
                .given()
                .get("http://localhost:8080/Gradle___mambu_internship_assignements_war/account/list")
                .then()
                .statusCode(200)
                .body("id", hasItems(4))
                .time(lessThan(1L), TimeUnit.SECONDS);
    }
}

package rest.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import rest.model.Account;
import rest.model.Transaction;

import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TransactionControllerTest {

    ObjectMapper objectMapper = new ObjectMapper();
    Long expectedId = 1L;

    @Test
    public void testGetTransactionReturnedIdValue() {
        Response res = RestAssured
                .given()
                .get("http://localhost:8080/Gradle___mambu_internship_assignements_war/transaction/1")
                .then().extract().response();
        String extracted = res.jsonPath().getString("id");
        assertTrue(extracted.equals("1"));
    }

    @Test
    public void testGetTransactionById() {
        RestAssured
                .given()
                .get("http://localhost:8080/Gradle___mambu_internship_assignements_war/transaction/" + expectedId)
                .then()
                .statusCode(200)
                .body("id", CoreMatchers.equalTo(1))
                .body("value", CoreMatchers.equalTo(100));
    }

    @Test
    public void testCreateNewTransaction() throws JsonProcessingException {
        Account account = new Account(100, "name", "address");
        Transaction transaction = new Transaction(108, account, 1000);
        RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(objectMapper.writeValueAsString(transaction))
                .post("http://localhost:8080/Gradle___mambu_internship_assignements_war/transaction")
                .then()
                .statusCode(200)
                .body("id", CoreMatchers.equalTo(108))
                .time(lessThan(1L), TimeUnit.SECONDS);
    }

    @Test
    public void testGetTransactionList() {
        RestAssured
                .given()
                .get("http://localhost:8080/Gradle___mambu_internship_assignements_war/transaction/list")
                .then()
                .statusCode(200)
                .body("id", hasItems(4))
                .time(lessThan(1L), TimeUnit.SECONDS);
    }
}

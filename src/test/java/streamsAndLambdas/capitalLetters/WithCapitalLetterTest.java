package streamsAndLambdas.capitalLetters;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.ArrayList;
import java.util.List;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class WithCapitalLetterTest {

    WithCapitalLetter withCapitalLetter = new WithCapitalLetter();
    List<String> listWithWordsAllLowerCase;
    List<String> listWithWordsAllUpperCase;
    List<String> nullList;

    @BeforeAll
    public void setup() {
        listWithWordsAllLowerCase = new ArrayList<>();
        listWithWordsAllUpperCase = new ArrayList<>();
        nullList = new ArrayList<>();
        listWithWordsAllLowerCase.add("word");
        listWithWordsAllLowerCase.add("list");
        listWithWordsAllUpperCase.add("WORD");
        listWithWordsAllUpperCase.add("LIST");


    }

    @Test
    public void capitalizeFirstLetterWhenWordsAreLowerSucces() {

        String extractedRes = withCapitalLetter.wordsWithCapitalLetter(listWithWordsAllLowerCase);
        String expectedRes = "Word,List";
        Assertions.assertEquals(expectedRes, extractedRes);

    }

    @Test
    public void capitalizeFirstLetterWhenWordsAreUpperSucces() {

        String extractedRes = withCapitalLetter.wordsWithCapitalLetter(listWithWordsAllUpperCase);
        String expectedRes = "Word,List";
        Assertions.assertEquals(expectedRes, extractedRes);

    }

    @Test
    public void capitalizeFirstLetterListNull() {
        String extractedRes = withCapitalLetter.wordsWithCapitalLetter(nullList);
        String expectedRes = "";
        Assertions.assertEquals(expectedRes, extractedRes);
    }


}

package streamsAndLambdas.coutVowels;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import streamsAndLambdas.countVowels.CountVowelsImpl;

import java.util.ArrayList;
import java.util.List;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class countVowelsTest {
    CountVowelsImpl countVowels = new CountVowelsImpl();
    List<String> list;

    @BeforeEach
    public void setup() {
        list = new ArrayList<>();
    }


    @Test
    public void countVowelsSucces() {
        list.add("Ana");
        list.add("Mere");
        int extractedRes = countVowels.countVowelsFromString(list);
        int expectedRes = 4;
        Assertions.assertEquals(expectedRes, extractedRes);
    }

    @Test
    public void countVowelsNoVowel() {
        list.add("xyz");
        list.add("zzz");
        int extractedRes = countVowels.countVowelsFromString(list);
        int expectedRes = 0;
        Assertions.assertEquals(expectedRes, extractedRes);
    }

    @Test
    public void countVowelsNullList() {
        int extractedRes = countVowels.countVowelsFromString(list);
        int expectedRes = 0;
        Assertions.assertEquals(expectedRes, extractedRes);
    }


}

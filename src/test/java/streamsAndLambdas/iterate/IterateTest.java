package streamsAndLambdas.iterate;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import streamsAndLambdas.iterating.Iterate;
import streamsAndLambdas.iterating.model.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class IterateTest {

    Iterate iterate = new Iterate();
    User u1;
    User u2;
    List<User> list;

    @BeforeEach
    public void setup() {
        list = new ArrayList<>();
        u1 = new User("A", "b");
        u2 = new User("Z", "x");
    }

    @Test
    public void returnListWithUserNamesForEachMethod() {
        list.add(u1);
        list.add(u2);

        List<String> extractedRes = iterate.iterateUsingForEach(list);
        List<String> expectedRes = new ArrayList<>(Arrays.asList("A", "Z"));
        Assertions.assertEquals(expectedRes, extractedRes);

    }

    @Test
    public void returnListWithUserNamesStreamMethod() {
        list.add(u1);
        list.add(u2);

        List<String> extractedRes = iterate.iterateUsingStreams(list);
        List<String> expectedRes = new ArrayList<>(Arrays.asList("A", "Z"));
        Assertions.assertEquals(expectedRes, extractedRes);
    }

    @Test
    public void returnListWithUserNamesStreamMethodNull() {

        List<String> extractedRes = iterate.iterateUsingStreams(list);
        List<String> expectedRes = new ArrayList<>();
        Assertions.assertEquals(expectedRes, extractedRes);
    }

    @Test
    public void returnListWithUserNamesForEachMethodNull() {

        List<String> extractedRes = iterate.iterateUsingForEach(list);
        List<String> expectedRes = new ArrayList<>();
        Assertions.assertEquals(expectedRes, extractedRes);
    }
}

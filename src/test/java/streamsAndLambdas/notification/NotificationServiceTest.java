package streamsAndLambdas.notification;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import streamsAndLambdas.notification.model.Email;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class NotificationServiceTest {

    NotificationService notificationService = new NotificationService();
    @Mock
    Email email = Mockito.mock(Email.class);
    NotificationSubscriber notificationSubscriber = new NotificationSubscriber();

    @Test
    public void testSendingEmail() {
        List<Email> emailList = new ArrayList<>();
        email = new Email("a", "b");
        email.setEmail("andrei@mambu.com");
        email.setMessage("SomeMessage");
        emailList.add(email);
        notificationService.getPublisher().subscribe(notificationSubscriber);
        notificationService.notificationProcess(emailList);
        notificationService.getPublisher().close();
        List<Email> extractedList = notificationSubscriber.getConsumedElements();

        Assertions.assertEquals(1, extractedList.size());
    }

}

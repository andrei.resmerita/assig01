package streamsAndLambdas.operations;

import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class OperationsTest {

    List<Integer> list;
    Operations operations = new Operations();

    @BeforeEach
    public void setup() {
        list = new ArrayList<>();
    }

    @Test
    public void getMaxValueFromList() {
        list.add(30);
        list.add(4000);
        list.add(100);
        int extractedRes = operations.maxValue(list);
        int expectedRes = 4000;
        Assertions.assertEquals(expectedRes, extractedRes);
    }

    @Test
    public void getMinValueFromList() {
        list.add(30);
        list.add(4000);
        list.add(100);
        int extractedRes = operations.minValue(list);
        int expectedRes = 30;
        Assertions.assertEquals(expectedRes, extractedRes);
    }

    @Test
    public void getAverageValueFromList() {
        list.add(30);
        list.add(4000);
        list.add(100);
        double extractedRes = operations.averageValue(list);
        double expectedRes = 1376.6666666666667;
        Assertions.assertEquals(expectedRes, extractedRes);
    }

    @Test
    public void getMaxNoElem() {
        NoSuchElementException exception = Assert.assertThrows(NoSuchElementException.class, () -> operations.maxValue(list));
        Assertions.assertNotNull(exception);
    }

    @Test
    public void getMinNoElem() {
        NoSuchElementException exception = Assert.assertThrows(NoSuchElementException.class, () -> operations.minValue(list));
        Assertions.assertNotNull(exception);
    }

    @Test
    public void getAverageNoElem() {
        NoSuchElementException exception = Assert.assertThrows(NoSuchElementException.class, () -> operations.averageValue(list));
        Assertions.assertNotNull(exception);
    }
}

package streamsAndLambdas.products.modelDataUtil;

import streamsAndLambdas.products.model.Category;
import streamsAndLambdas.products.model.Product;

import java.util.ArrayList;
import java.util.List;

public class CreateModelsTest {

    public static List<Product> createListOfProductsDifferentProducts() {
        List<Product> products = new ArrayList<>();
        Product p1 = new Product("Laptop", Category.LAPTOP, 2000.00);
        Product p2 = new Product("TV", Category.TV, 3000.00);
        Product p3 = new Product("Phone", Category.SMARTHPHONE, 1000.00);

        products.add(p1);
        products.add(p2);
        products.add(p3);
        return products;

    }

    public static List<Product> createListOfProductsSameProducts() {
        List<Product> products = new ArrayList<>();
        Product p1 = new Product("Laptop", Category.LAPTOP, 2000.00);
        Product p2 = new Product("Laptop", Category.LAPTOP, 2000.00);
        Product p3 = new Product("Laptop", Category.LAPTOP, 2000.00);

        products.add(p1);
        products.add(p2);
        products.add(p3);
        return products;

    }

}

package streamsAndLambdas.products.service.applyDiscountPerCategoryTest;

import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import streamsAndLambdas.products.model.Category;
import streamsAndLambdas.products.model.Product;
import streamsAndLambdas.products.modelDataUtil.CreateModelsTest;
import streamsAndLambdas.products.service.applyDiscountPerCategory.ApplyDiscountPerCategoryPerCategoryImpl;

import java.util.ArrayList;
import java.util.List;

public class ApplyDiscountPerCategoryTest {

    ApplyDiscountPerCategoryPerCategoryImpl applyDiscountPerCategoryPerCategory = new ApplyDiscountPerCategoryPerCategoryImpl();


    @Test
    public void testApplyDiscountForACategorySucces() {
        List<Product> randomList = CreateModelsTest.createListOfProductsDifferentProducts();
        List<Product> extractedRes = applyDiscountPerCategoryPerCategory.productsWithDiscount(randomList, Category.TV, 10);
        List<Product> expectedRes = new ArrayList<>();
        Product expectedProduct = new Product("TV", Category.TV, 2700.00);
        Product p1 = new Product("Laptop", Category.LAPTOP, 2000.00);
        Product p3 = new Product("Phone", Category.SMARTHPHONE, 1000.00);
        expectedRes.add(p1);
        expectedRes.add(expectedProduct);

        expectedRes.add(p3);

        Assertions.assertEquals(expectedRes, extractedRes);

    }

    @Test
    public void testApplyDiscountNoCategory() {
        List<Product> sameProducts = CreateModelsTest.createListOfProductsSameProducts();
        List<Product> extractedRes = applyDiscountPerCategoryPerCategory.productsWithDiscount(sameProducts, Category.TV, 10);
        List<Product> expectedRes = CreateModelsTest.createListOfProductsSameProducts();
        Assertions.assertEquals(expectedRes, extractedRes);

    }

    @Test
    public void ApplyDiscountNullList() {
        NullPointerException nullPointerException = Assert.assertThrows(NullPointerException.class, () -> applyDiscountPerCategoryPerCategory.productsWithDiscount(null, Category.TV, 10));
        Assertions.assertNotNull(nullPointerException);
    }

}

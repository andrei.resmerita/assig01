package streamsAndLambdas.stringFiltering;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class StringFilteringTest {

    List<String> list;
    StringFiltering stringFiltering = new StringFiltering();

    @BeforeEach
    public void setup() {
        list = new ArrayList<>();
    }

    @Test
    public void wordsStartingWithNTest() {
        list.add("natriu");
        list.add("ana");

        List<String> extractedRes = stringFiltering.wordsStartingWithN(list);
        List<String> expectedRes = new ArrayList<>(Collections.singletonList("natriu"));

        Assertions.assertEquals(expectedRes, extractedRes);
    }

    @Test
    public void NoWordsStartingWithNTest() {
        list.add("lamaie");
        list.add("ana");

        List<String> extractedRes = stringFiltering.wordsStartingWithN(list);
        List<String> expectedRes = new ArrayList<>();

        Assertions.assertEquals(expectedRes, extractedRes);
    }

    @Test
    public void wordsEndingWithATest() {
        list.add("ana");
        list.add("string");

        List<String> extractedRes = stringFiltering.wordsEndingWithA(list);
        List<String> expectedRes = new ArrayList<>(Collections.singletonList("ana"));

        Assertions.assertEquals(expectedRes, extractedRes);
    }

    @Test
    public void NoWordsEndingWithATest() {
        list.add("aer");
        list.add("string");

        List<String> extractedRes = stringFiltering.wordsEndingWithA(list);
        List<String> expectedRes = new ArrayList<>();

        Assertions.assertEquals(expectedRes, extractedRes);
    }

    @Test
    public void wordsLongerThan5Letters() {
        list.add("longword");
        list.add("apa");

        List<String> extractedRes = stringFiltering.wordsLongerThan5Letters(list);
        List<String> expectedRes = new ArrayList<>(Collections.singletonList("longword"));

        Assertions.assertEquals(expectedRes, extractedRes);
    }

    @Test
    public void NoWordsLongerThan5Letters() {
        list.add("aer");
        list.add("apa");

        List<String> extractedRes = stringFiltering.wordsLongerThan5Letters(list);
        List<String> expectedRes = new ArrayList<>();

        Assertions.assertEquals(expectedRes, extractedRes);
    }

    @Test
    public void wordsWithAtLeast3Vowels() {
        list.add("maria");
        list.add("apa");

        List<String> extractedRes = stringFiltering.wordsWithAtLeast3Vowels(list);
        List<String> expectedRes = new ArrayList<>(Collections.singletonList("maria"));

        Assertions.assertEquals(expectedRes, extractedRes);
    }

    @Test
    public void NowordsWithAtLeast3Vowels() {
        list.add("aer");
        list.add("apa");

        List<String> extractedRes = stringFiltering.wordsWithAtLeast3Vowels(list);
        List<String> expectedRes = new ArrayList<>();

        Assertions.assertEquals(expectedRes, extractedRes);
    }

}

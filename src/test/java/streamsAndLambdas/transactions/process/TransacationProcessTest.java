package streamsAndLambdas.transactions.process;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import streamsAndLambdas.transactions.model.Account;
import streamsAndLambdas.transactions.model.Transaction;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TransacationProcessTest {

    TransactionProcess transactionProcess = new TransactionProcess();
    List<Account> accountList;
    List<Transaction> transactionList1;
    List<Transaction> transactionList2;

    @BeforeEach
    public void setup() {
        accountList = new ArrayList<>();
        transactionList1 = new ArrayList<>();
        transactionList2 = new ArrayList<>();

    }

    @Test
    public void getAverageTransactionAmountTestSuccess() throws ParseException {
        Date date = new SimpleDateFormat("dd-MM-YYYY").parse("22-05-2021");
        Account a1 = new Account("1", "a", "r", transactionList1);
        Transaction t1 = new Transaction(1, 100, date, a1);
        Transaction t2 = new Transaction(2, 300, date, a1);
        Transaction t3 = new Transaction(3, 50, date, a1);
        transactionList1.add(t1);
        transactionList1.add(t2);
        transactionList1.add(t3);
        accountList.add(a1);
        Account a2 = new Account("2", "x", "y", transactionList2);
        Transaction t22 = new Transaction(1, 400, date, a1);
        Transaction t23 = new Transaction(2, 50, date, a1);
        Transaction t24 = new Transaction(3, 250, date, a1);
        transactionList2.add(t22);
        transactionList2.add(t23);
        transactionList2.add(t24);
        accountList.add(a2);
        double extractedRes = transactionProcess.getAverageTransactionAmount(accountList);
        double expectedRes = 191.66666666666669;

        Assertions.assertEquals(expectedRes, extractedRes);
    }

    @Test
    public void getTop3TransactionFromAllAccountsTestSucces() throws ParseException {
        Date date = new SimpleDateFormat("dd-MM-YYYY").parse("22-05-2021");
        Account a1 = new Account("1", "a", "r", transactionList1);
        Transaction t1 = new Transaction(1, 100, date, a1);
        Transaction t2 = new Transaction(2, 300, date, a1);
        Transaction t3 = new Transaction(3, 50, date, a1);
        transactionList1.add(t1);
        transactionList1.add(t2);
        transactionList1.add(t3);
        accountList.add(a1);
        Account a2 = new Account("2", "x", "y", transactionList2);
        Transaction t22 = new Transaction(1, 400, date, a1);
        Transaction t23 = new Transaction(2, 50, date, a1);
        Transaction t24 = new Transaction(3, 250, date, a1);
        transactionList2.add(t22);
        transactionList2.add(t23);
        transactionList2.add(t24);
        accountList.add(a2);
        List<Transaction> extractedRes = transactionProcess.getTop3TransactionsFromAllAccounts(accountList);
        List<Transaction> expectedRes = new ArrayList<>();
        expectedRes.add(t22);
        expectedRes.add(t2);
        expectedRes.add(t24);

        Assertions.assertEquals(expectedRes, extractedRes);

    }

    @Test
    public void totalTransactionAmountOfAMonthTestSucces() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date dt1 = sdf.parse("21/12/2021");
        Date dt2 = sdf.parse("15/02/2021");
        Date dt3 = sdf.parse("15/04/2021");
        Date dt4 = sdf.parse("15/04/2021");
        Account a1 = new Account("1", "a", "r", transactionList1);
        Transaction t1 = new Transaction(1, 100, dt1, a1);
        System.out.println(t1.toString());
        Transaction t2 = new Transaction(2, 300, dt2, a1);
        Transaction t3 = new Transaction(3, 50, dt3, a1);
        transactionList1.add(t1);
        transactionList1.add(t2);
        transactionList1.add(t3);
        accountList.add(a1);
        Account a2 = new Account("2", "x", "y", transactionList2);
        Transaction t22 = new Transaction(1, 400, dt4, a1);
        Transaction t23 = new Transaction(2, 50, dt1, a1);
        Transaction t24 = new Transaction(3, 250, dt2, a1);
        transactionList2.add(t22);
        transactionList2.add(t23);
        transactionList2.add(t24);
        accountList.add(a2);
        Long extractedRes = transactionProcess.totalTransactionAmountOfAMonth(accountList, dt4);
        Long expectedRes = 450L;

        Assertions.assertEquals(expectedRes, extractedRes);
    }

    @Test
    public void getAccountAndTransactionInAMapTest() throws ParseException {
        Date date = new SimpleDateFormat("dd-MM-YYYY").parse("22-05-2021");
        Account a1 = new Account("1", "a", "r", transactionList1);
        Transaction t1 = new Transaction(1, 100, date, a1);
        Transaction t2 = new Transaction(2, 300, date, a1);
        Transaction t3 = new Transaction(3, 50, date, a1);
        transactionList1.add(t1);
        transactionList1.add(t2);
        transactionList1.add(t3);
        accountList.add(a1);
        Account a2 = new Account("2", "x", "y", transactionList2);
        Transaction t22 = new Transaction(1, 400, date, a1);
        Transaction t23 = new Transaction(2, 50, date, a1);
        Transaction t24 = new Transaction(3, 250, date, a1);
        transactionList2.add(t22);
        transactionList2.add(t23);
        transactionList2.add(t24);
        accountList.add(a2);
        Map<String, List<Transaction>> extractedRes = transactionProcess.getAccountAndTransactions(accountList);
        Map<String, List<Transaction>> expectedRes = new HashMap<>();
        expectedRes.put(a1.getNumber(), transactionList1);
        expectedRes.put(a2.getNumber(), transactionList2);
        Assertions.assertEquals(expectedRes, extractedRes);

    }
}

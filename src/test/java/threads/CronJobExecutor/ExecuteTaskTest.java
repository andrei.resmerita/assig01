package threads.CronJobExecutor;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ExecuteTaskTest {

    ExecuteTasks executeTasks;
    File file;
    Task task;
    ReadFromJson readFromJson;

    @BeforeEach
    public void setup() throws InterruptedException, ExecutionException, IOException {
        executeTasks = new ExecuteTasks();
        file = new File("src/cron.json");
        readFromJson = new ReadFromJson();
        task = new Task(readFromJson.readFromJson(file));


    }

    @Test
    public void doingTasksCorrectHourTestSucces() throws InterruptedException, ExecutionException, IOException {
        task.setHm("13:10");
        boolean extractedRes = executeTasks.doTask(task);
        boolean expectedRes = true;
        Assertions.assertEquals(expectedRes, extractedRes);

    }

    @Test
    public void doingTasksWrongHourTestSucces() throws InterruptedException, ExecutionException, IOException {
        task.setHm("22:10");
        boolean extractedRes = executeTasks.doTask(task);
        boolean expectedRes = true;
        Assertions.assertNotEquals(expectedRes, extractedRes);

    }

}

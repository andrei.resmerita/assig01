package threads.deadlock;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import threads.deadlocks.Deadlock;
import threads.deadlocks.IAmDoingJobThread;
import threads.deadlocks.IWillBeInDeadLockThread;
import threads.syncronized.Performance;


public class DeadlockTest {
    Deadlock deadlock = new Deadlock();
    Performance n = new Performance();
    IAmDoingJobThread iAmDoingJobThread = new IAmDoingJobThread(n, 10);
    IWillBeInDeadLockThread iWillBeInDeadLockThread = new IWillBeInDeadLockThread(n, 10);


    @Test
    public void deadlockSucces() throws InterruptedException {

        boolean res = deadlock.startingDeadlock(iAmDoingJobThread, iWillBeInDeadLockThread, n);
        Assertions.assertTrue(res);
    }


}

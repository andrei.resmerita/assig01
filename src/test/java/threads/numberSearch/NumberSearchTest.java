package threads.numberSearch;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class NumberSearchTest {
    SearchForNumber searchForNumber = new SearchForNumber();
    List<Integer> list;
    Random r;
    private int n;
    private int start;
    private int end;

    @BeforeEach
    public void setup() {
        r = new Random();
        start = 0;
        list = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            list.add(r.nextInt(1000));
        }

        end = list.size() - 1;
    }

    @Test
    public void searchForANumberSucces() throws ExecutionException, InterruptedException {
        n = 100;
        boolean extractedRes = searchForNumber.searchForNumber(list, n, start, end);
        boolean expectedRes = true;
        Assertions.assertEquals(expectedRes, extractedRes);
    }

    @Test
    public void searchForANumberNoNumberFound() throws ExecutionException, InterruptedException {
        n = 3000;
        boolean extractedRes = searchForNumber.searchForNumber(list, n, start, end);
        boolean expectedRes = false;
        Assertions.assertEquals(expectedRes, extractedRes);
    }
}

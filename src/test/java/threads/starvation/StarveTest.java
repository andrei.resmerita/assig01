package threads.starvation;

import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StarveTest {
    Starve starve = new Starve();

    @Test
    public void testStarvationSucces() {
        RuntimeException runtimeException = Assert.assertThrows(RuntimeException.class, () -> starve.doingStarvation());
        Assertions.assertNotNull(runtimeException);
    }
}

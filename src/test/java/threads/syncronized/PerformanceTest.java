package threads.syncronized;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PerformanceTest {
    Performance n = new Performance();


    @Test
    public void ThreadWaitingForTheOtherWaitsLongTime() throws InterruptedException {
        Thread1 t = new Thread1(n, 50);
        Thread2 t2 = new Thread2(n, 10);
        boolean extractedRes = n.getTime(t, t2);
        boolean expecteRes = true;
        Assertions.assertEquals(expecteRes, extractedRes);

    }

    @Test
    public void ThreadWaitingForTheOtherWaitsLongTimeFail() throws InterruptedException {
        Thread1 t = new Thread1(n, 5);
        Thread2 t2 = new Thread2(n, 1);
        boolean extractedRes = n.getTime(t, t2);
        boolean expecteRes = false;
        Assertions.assertEquals(expecteRes, extractedRes);

    }

    ;
}
